<div class="card_msj">    
<div class='mensaje01'>

<p style="text-indent: 10px; margin:2px;">
<b>MUY IMPORTANTE!!!!!</b>
<br/>
<p>
Para iniciar oficialmente el trámite de reconocimiento de equivalencias externas (desde universidad de origen a UCASAL) deben cumplirse los siguientes requisitos:
</p>
<ul>
    <li>Matrícula abonada,</li>
    <li>Legajo digital completo,</li>
    <li>La carpeta de equivalencias puede ser presentada en formato papel o bien en formato digital. Debe estar dirigida a la Universidad Católica de Salta y debe contener: Certificado analítico, plan de estudios, programas, certificado de libre sanción disciplinaria y baja de matrícula.</li>
</ul>
<br/>              
<p>
Si presenta la carpeta en formato papel, debe entregarla en la Dirección de Alumnos presencial (Campus Castañares) / distancia (Anexo Pellegrini 790) – Salta, Capital o bien, en la Sede a la cual pertenece.
</p>
<br/>
<p>Si presenta la carpeta en formato digital:</p>
<ul>
    <li>Si es alumno del modo presencial debe enviarla por mail a la dirección equivalencias.presencial@ucasal.edu.ar</li>
    <li> Si es alumno del modo distancia debe enviarla por mail a la dirección equivalencias.distancia@ucasal.edu.ar</li>
</ul>        
El otorgamiento de Equivalencias se rige según el Reglamento de la Institución. La Universidad Católica de Salta se reserva el derecho sobre el otorgamiento de
las materias solicitadas a ser aprobadas por equivalencias de estudios, sujeto a la revisión de la documentación presentada y a la vigencia de las materias rendidas.
</p>
</div>
</div>
<style>
.espacio {    
width: 100px ;
height: 5px;
}
</style>

<div class="espacio"></div>