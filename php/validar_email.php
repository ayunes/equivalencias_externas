<?php
    session_start();

    if (isset($_POST['carrera_nombre_o'])) {    
        $_SESSION['carrera_nombre_o'] = $_POST['carrera_nombre_o'];
    }

    if (isset($_POST['modo_d'])) {    
        $nombre_carrera = $_POST['carrera_d'];    
        $nombre_modo    = $_POST['modo_d'];
        $codigo_carrera = $_POST['codigo_carrera'];           
    }
?>

<!-- <div class="contenedor_formulario"> -->

    <div class="contenedor_titulo_formulario">
        <span class="titulo_formulario"> Solicitar Información </span> <br> 
        <span class="text_titulo">  No se encotraron equivalencias en la carrera solicitada, ingrese los siguientes datos para continuar.  </span> 
    </div>

    <div class="form_selector">

        <div class="card-footer">
            <div class="col-auto text-center titulo-carrera"><b>DATOS DEL ALUMNO</b></div>
        </div>

        <div class="card-body">
        <form action='proc_expediente.php' method="POST" enctype="multipart/form-data" id="form_expediente" >    
            <div class="row mb-3">
                <div class="col">
                    <div class='label-select'><b>Nombre</b></div>
                    <input type="text" class="form-control" id="nombre" placeholder="Nombre/s del solicitante" name='nombre'  required="required"> 
                </div>

                <div class="col">
                    <div class='label-select'><b>Apellido</b></div>
                    <input type="text" class="form-control" id="apellido" placeholder="Apellido/s del solicitante" name='apellido'  required="required">
                </div>
            </div>
            <div class=" row mb-3">        
            <div class="col">
                    <div class='label-select'><b>Email</b></div>
                    <input type="email" class="form-control" id="email" name='email' placeholder="Ingrese su email"  required="required" >
                </div>

                <div class="col">
                    <div class='label-select'><b>Nº de Documento</b></div>
                    <input type="hidden"  name='t_docu' value="1">
                    <input type="text" class="form-control" id="n_docu" placeholder="Número de DNI" name='n_docu' required="required" >
                </div>        
            </div>    
            <br>
            <div class="mb-3">
                <div class="row">
                    <div class="col">
                        <div class='label-select'><b>Carrera de Interes</b></div>
                        <input type="text" class="form-control" name="nombre_carrera" value="<?= $nombre_carrera; ?>" readonly />
                        <!--<input type="hidden" class="form-control" name="codigo_sector" value="<?= $codigo_sector; ?>" readonly />-->
                        <input type="hidden" class="form-control" name="codigo_carrera" value="<?= $codigo_carrera; ?>" readonly />
                    </div>
                    <div class="col">
                        <div class='label-select'><b>Modo</b></div>
                        <input type="text" class="form-control" name="nombre_modo" value="<?= $nombre_modo; ?>" readonly />
                    </div>
                </div>
            </div>
            <br>
            <div class="mb-3">
                <div class='label-select'><b>Si posee el Certificado Analítico de su carrera adjuntelo aquí. </b> (solo archivos formato pdf, tamaño maximo: 15 mb) </div>
                <input class="form-control" type="file" id="formFile" name="certificado" accept="application/pdf">
            </div>
            <br />
            <div class="mb-3">
                <div class='label-select'><b>Si posee el Plan de Estudio de la Carrera adjuntelo aquí. </b> (solo archivos formato pdf, tamaño maximo: 15 mb) </div>
                <input class="form-control" type="file" id="formFile" name="plan" accept="application/pdf">
            </div>
            <br />
            <div class="mb-3">
                <div class='label-select'><b>Observaciones</b></div>
                <textarea class="form-control" id="observaciones" maxlength="260"  rows="3" name='observaciones'></textarea>
            </div>

            <div class="mb-3">
                    <div class="form-check">
                        <label class="form-check-label" for="flexCheckDefault">
                            <p style="color: black;">
                            El otorgamiento de Equivalencias se rige según el Reglamento de la Institución. La Universidad Católica de Salta se reserva el derecho sobre el otorgamiento de
                            las materias solicitadas a ser aprobadas por equivalencias de estudios, sujeto a la revisión de la documentación presentada y a la vigencia de las materias rendidas.
                            </p>
                        </label>            
                    </div>        
                <br>
                <div class="text-center" style="color: black;"> 
                    Aceptar terminos y condiciones <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault" checked require>
                </div>
                
                <input type="hidden" name="tipo_form" value="info" />
                <input type="hidden" name="codigo_sector" value="<?=$_SESSION['codigoSector']; ?>" />	

            </div>    

            <br /> <!-- Boton Simular -->
            <div class="col-auto text-center boton-simular">
                <div id="btn_hide">
                    <button class="btn btn-primary" type="submit" name="enviar" id="btn_enviar">ENVIAR DATOS</button>
                </div>                
            </div>
        </form>
        </div><!-- end card-body -->

    </div><!-- end form_selector -->

<!-- </div> -->

<script>
    $(document).ready(function() {        
        $(location).attr('href','#form-info'); 

        $('.frm_admision').hide();
        $('.mostrar_mensaje').hide();

        setTimeout(function() {
            $("#nombre").focus();
        }, 500);
             
    });
</script>