<div class="form_selector">
    
    <?php    
    /*
    -------------------------------------------------------------------------
        Listado de carreras de grado / carrera destino- SAG BACKEND
    -------------------------------------------------------------------------
    */        
  
    $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_URL => 'http://docker-server.ucasal.edu.ar:10019/v1/carreras?tipo=Grado%2CPregrado',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'GET',
    ));

    $response = curl_exec($curl);

    curl_close($curl);
        
    $carreraGrado = json_decode($response, true);   

    ?>
    <style>
        
    </style>
    <div class="card-body">
        <div class="mb-3">
            <select class="lGanteCon form-select" aria-label="Ingrese la Facultad" name="carrera_dest" id="lista1" required="required" autofocus="autofocus">
                <option value="">SELECCIONE CARRERA</option>
                <?php    
                        
                foreach ($carreraGrado as $row) {
                    print '<option value="' . $row['codigoCarrera'] . '">' . $row['nombreCarrera'] . '</option>';                    
                } 
                

                /*
                foreach ($carreraGrado as $row) {
                    print '<option value="' . $row['codigo'] . '">' . $row['carrera'] . '</option>';                    
                } 
                */               
                ?>                
            </select>
        </div><!-- mb3 -->
                
        <div class="mb-3">
            <!--<div  id="lista2" ></div>-->
            <select class="lGanteSin form-select" aria-label="Modo" name="modo_dest" id="lista2" required="require">
                <option value="">SELECCIONE MODO</option>
            </select>
        </div>
        
    </div><!-- card-body -->

</div><!-- form selector -->
 
 <br>