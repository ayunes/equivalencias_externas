<div class="form_selector">

    <div class="card-body">

        <div class="des_origen">

            <div class="mb-3">
                <!--<div id="lista4"></div>-->
                <select class="lGanteSin form-select" aria-label="Ingrese tipo de Institución" name="tipo_inst" id="lista4" require>
                    <option>SELECCIONE TIPO DE INSTITUCION</option>
                </select>
            </div><!-- mb-3 -->
            
            <div class="mb-3">
                <!-- <div id="lista5"></div> -->
                <select class="lGanteCon form-select" aria-label="Default" name="inst_origen" id="lista5" require>
                    <option>SELECCIONE INSTITUCION</option>
                </select>
            </div><!-- mb-3 -->

            <div class="mb-3"> 
                <!--<div id="lista6"></div>-->
                <select class="lGanteSin form-select" aria-label="Default" name="id_carrera_origen" id="lista6" require>
                    <option>SELECCIONE CARRERA</option>
                </select>
            </div><!-- mb-3 -->

            <div class="mb-3">                  
                <!-- <div id=lista7></div>  -->
                <select class="lGanteSin form-select" aria-label="Default" name="plan_estudio" id="lista7" require>
                    <option>SELECCIONE PLAN DE ESTUDIO</option>
                </select>
            </div><!-- mb-3 -->

        </div>    <!--div origen a ocultar -->

    </div><!-- card body -->
    
    <!-- Si no encuentra equivalencias muestra la siquiente ventana -->
    <div class="des_no_encuentra">
                        
        <div class="card-body">

            <div class="mb-3">                    
                <input type="text" class="form-control" placeholder="SELECCIONE TIPO DE INSTITUCION" readonly>                
            </div><!-- mb-3 -->
            
            <div class="mb-3">                    
                <input type="text" class="form-control" placeholder="SELECCIONE INSTITUCION" readonly>
            </div><!-- mb-3 -->

            <div class="mb-3"> 
                <input type="text" class="form-control" placeholder="SELECCIONE CARRERA" readonly>
            </div><!-- mb-3 -->

            <div class="mb-3">                
                <input type="text" class="form-control" placeholder="SELECCIONE PLAN DE ESTUDIO" readonly> 
            </div><!-- mb-3 -->

        </div><!-- card body -->    
        
</div><!-- no_se_muestra -->