<?php

use FontLib\Table\Type\name;

if (isset($_POST['carrera_d'])) { 
    $codigo_carrera    = $_POST['codigo_carrera'];
    $nombre_carrera    = $_POST['carrera_d'];            
    $modo              = $_POST['modo_d'];
}
?>
<br>

<div class="card border-secondary">

    <div class="card-header bg-light">
        <div class="col-auto text-center titulo-carrera"><b>Ingrese su email para continuar</b></div>        
    </div>
    
    <div class="card-body">

        <form action='proc_postulantes.php' method="POST" enctype="multipart/form-data">
            <div class="row mb-3">
                <div class="col">
                    <div class='label-select'><b>Nombre</b></div>
                    <input type="text" class="form-control" id="nombre" placeholder="Nombre completo del solicitante" name='nombre' value="" required=" required"> <!-- required=" required" -->
                </div>
            </div>    
            
            <div class="mb-3">
                <div class='label-select'><b>Email</b></div>
                <input type="email" class="form-control" id="email" name='email' placeholder="Ingrese su email" value="" required>
            </div>          

            <br />
            <div class="col-auto text-center boton-simular">
                <div id="btn_hide">
                    <button class="btn btn-primary " type="submit" name="enviar" id="id_eviar">ENVIAR</button>
                </div>

                <div id="msj_procesando">
                    <p>Procesando datos, espere por favor.</p>
                </div>
            </div>
        </form>        
    </div><!-- card body -->   
    
</div><!-- card -->

<script>
    $(document).ready(function() {    

        setTimeout(function() {
            $("#nombre").focus();
        }, 500); 

        $("#msj_procesando").hide();       

    });

    $("#btn_enviar").on("click", function() {       
       $("#btn_hide").hide();     
       $("#msj_procesando").show();
    });
    
   
    $("#btn-numero3-info").on("click", function() {
        $.ajax({
            type: "POST",
            url: "script/ajax_sin_equivalencias.php",
            data: {                        
                "carrera_d": $('#lista1 option:selected').html(),
                "codigo_carrera": $('#lista1 option:selected').val(),
                "modo_d":$('#lista2 option:selected').html()
            },
            success: function(r) {
                $('#formulario-info').html(r);
            }
        });
    });
    
</script>