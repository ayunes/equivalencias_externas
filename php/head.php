<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title> Consultas de Equivalencias </title>
        
        <!-- Favicon-->
        <link rel="icon" type="image/x-icon" href="assets/favicon.ico" />
        
        <!-- Core theme CSS (includes Bootstrap)-->
        <link href="css/styles.css" rel="stylesheet" />     
        
        <!-- TOAST -->      
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.0.1/css/toastr.css" rel="stylesheet"/>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.0.1/js/toastr.js"></script>
        
        <!-- JQUERY -->
        <!--        
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
        <script src="js/jquery3.js"></script>
        -->
        
        
        <!-- Select2 Form -->        
        <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />        
        <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script> 
        

        <!-- Sweet Alert 2 -->    
        <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
        <!--<script src="sweetalert2.all.min.js"></script>-->
    

        <!-- Sweet Alert -->
        <!--
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
        -->
        
    </head>
    
    <body style="background-color:#f3f3f3;" >    
    
<?php
    session_start();    
    header ('Cache-Control: no-store, no-cache, must-revalidate, max-age = 0');

    // Desactivar toda notificación de error
    //error_reporting(0);

    include('config.php');
?>