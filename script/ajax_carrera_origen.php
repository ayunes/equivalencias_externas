<?php
session_start();
if (isset($_POST['id_carrera'])) {

    include('../php/config.php');
        
    $id_sector      = $_SESSION['codigoSector'];
    $id_carrera     = $_POST['id_carrera'];
    $modo_dest      = $_POST['modo_dest'];
    $tipo_inst      = $_POST['tipo_inst'];    
    $inst_origen    = $_POST['inst_origen'];

    /*
    print 'Sector: '.$id_sector;
    print '<br>';
    print 'Carrera: '.$id_carrera;
    print '<br>';
    print 'Modo: '.$modo_dest;
    print '<br>';
    print 'Tipo Inst: '.$tipo_inst;    
    print '<br>';
    print 'Institución: '.$inst_origen;
    print '<br>';  
    */
  
    /*------ JAVA -  Listado de carreras externas de UCASAL ------*/

    $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_URL => 'http://'.$html_link.'/instituciones-externas/carreras?carreraDestino='.$id_carrera.'&instituto='.$inst_origen,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'GET',
    ));

    $response = curl_exec($curl);

    curl_close($curl);


    //--- Proceso JSON  ----------------------------------------------------------------------------------------------------------
    $carrera = json_decode($response, true);
    $html = "";
    $html .= '<option value="0">SELECCIONE CARRERA ORIGEN</option>';
    foreach ($carrera as $row) {            
        $html .= '<option value="' . $row['codigo'] . '">' . strtoupper($row['carrera']) . '</option>';    
    } 
    print $html;
} else {
    print '<option value="0">CARRERA NO LISTADA</option>';
}//END IF
