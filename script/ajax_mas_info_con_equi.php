<?php
session_start();

//include('../php/control_datos_ingreso.php');  

if (isset($_POST['modo_d'])) {    
    $nombre_carrera = $_POST['carrera_d'];    
    $nombre_modo    = $_POST['modo_d'];
    $codigo_carrera = $_POST['codigo_carrera'];   
    $codigo_sector  = $_SESSION['codigoSector'];
    $nombre         = $_SESSION['nombre'];
    $email          = $_SESSION['email'];
}
?>

<a name="solicitar_info"></a>

<div class="contenedor_titulo_formulario">
    <span class="titulo_formulario"> Solicitar Información </span> <br> 
    <span class="text_titulo"> Complete  por favor los siguientes datos para que una autoridad de la Unidad Académica correspondiente se contacte con usted. </span> 
</div>

<div class="form_selector">

    <div class="card-footer">
        <div class="col-auto text-center titulo-carrera"><b>DATOS DEL ALUMNO</b></div>
    </div>

    <div class="card-body">
        <?php
            include('../php/inputs_formulario_mas_info.php');
        ?>
    </div><!-- end card-body -->

</div><!-- end form_consulta -->

<script type="text/javascript">
    
    $(document).ready(function() {
        //$(location).attr('href','#solicitar_info');   
        $(location).attr('href','#marca-formulario-insc');   
        

        setTimeout(function() {
            $("#apellido").focus();
        }, 500);
    });

</script>