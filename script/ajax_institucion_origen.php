<?php
session_start();

include('../php/config.php');

if (isset($_POST['id_carrera'])) {
  $id_sector  = $_SESSION['codigoSector'];
  $id_carrera = $_POST['id_carrera'];   
  $modo_dest  = $_POST['modo_dest'];   
  $tipo_inst  =  $_POST['tipo_inst']; //'Universidad';
  /* JAVA - Institución */
  $curl = curl_init();

  curl_setopt_array($curl, array(
    CURLOPT_URL => 'http://'.$html_link.'/instituciones-externas/lista?carrera='.$id_carrera.'&tipo='.$tipo_inst,
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => '',
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 0,
    CURLOPT_FOLLOWLOCATION => true,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => 'GET',
  ));

  $response = curl_exec($curl);
  curl_close($curl);


  //--- Proceso JSON ---------------------------------------------------
  $inst = json_decode($response, true);

  $html = "";
  $html .= '<option value="0">SELECCIONE INSTITUCION</option>';
  $html .= '<option value="9">OTRA INSTITUCIÓN </option>';

  foreach ($inst as $row) {
    $html .= '<option value="' . $row['codigo'] . '">' . strtoupper($row['institucion']) . '</option>';    
  } 
  
  print $html;
} else {
  print '<option value="9">INSTITUCION NO ENCONTRADA</option>';
}//END IF
