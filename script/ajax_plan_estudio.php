<?php
session_start();

include('../php/config.php');

if (isset($_POST['id_carrera'])) {
    $id_sector  = $_SESSION['codigoSector'];
    $id_carrera = $_POST['id_carrera'];    
    $modo_dest  = $_POST['modo_dest'];
    $tipo_inst  = $_POST['tipo_inst']; 
    $inst_origen  = $_POST['inst_origen'];    
    $id_carrera_origen  = $_POST['id_carrera_origen'];
    
    $_SESSION['carrera_nombre_o'] = $_POST['carrera_nombre_o'];
       
    /* JAVA - PLAN DE ESTUDIO */
    $curl = curl_init();
    
    curl_setopt_array($curl, array(
        CURLOPT_URL => 'http://'.$html_link.'/instituciones-externas/planes?institucion='.$inst_origen.'&carreraOrigen='.$id_carrera_origen.'&carreraDestino='.$id_carrera,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'GET',
    ));

    $response = curl_exec($curl);

    curl_close($curl);

    
    //--- Procesar JSON -------------------------------------------------------------------------------------------------------------
    $plan = json_decode($response, true);
    $html = "";
    $html .= '<option value="0">SELECCIONE PLAN DE ESTUDIO</option>';
    $html .= '<option value="0">OTRO PLAN</option>';

    foreach ($plan as $row) {         
        $plan_estudio = $row['codigo'];
        $html .= '<option value="' . $plan_estudio . '">' . strtoupper($plan_estudio) . '</option>';
        
    }
    print $html;
} else {
    print '<option value="0">PLAN NO ENCONTRADO</option>';
}//END IF