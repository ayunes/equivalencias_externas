<?php
session_start();

use phpDocumentor\Reflection\Location;

include('../php/config.php');

if(isset($_POST['carrera_d'])){	       
    $codigoSector  = $_SESSION['codigoSector']; //$_POST['id_sector'] 3
    $codigoCarrera = $_POST['carrera_d']; //16    
    $modo_dest  = $_POST['modo_d']; //0
    
    //--- JAVA Tipo de Institución -----------------------------------------------------
    $curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_URL => 'http://'.$html_link.'/instituciones-externas/tipos?carrera='.$codigoCarrera,
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => '',
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 0,
      CURLOPT_FOLLOWLOCATION => true,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => 'GET',
    ));

    $response = curl_exec($curl);

    curl_close($curl);

    //---- Procesa --------------------------------------------------------------------------
 
    $tipo = json_decode($response, true);
    
    $html = "";
    $html .= '<option value="0">SELECCIONE TIPO DE INSTITUCION</option>';   
    $html .= '<option value="0">--.--</option>';
    
    foreach ($tipo as $row) {		      
      $html.='<option value="'.$row['tipo'].'">'. strtoupper($row['tipo']).'</option>';
    }     

} else {
	$html .= '<option value="0">--.--</option>';
  
}//END IF

print $html;
?>