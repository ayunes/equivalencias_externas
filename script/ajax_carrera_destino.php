<?php 
if(isset($_POST['id_sector'])){
	
	include('../php/config.php');
	
	$id_sector = $_POST['id_sector'];

	/* ORDS - Obtiene el codigo de sector */

	$curl = curl_init();

	curl_setopt_array($curl, array(
	  CURLOPT_URL => 'https://sistemas.ucasal.edu.ar/'.$srv.'/web/equivalencias-externas/codigoSectorDestino/'.$id_sector.'',
	  CURLOPT_RETURNTRANSFER => true,
	  CURLOPT_ENCODING => '',
	  CURLOPT_MAXREDIRS => 10,
	  CURLOPT_TIMEOUT => 0,
	  CURLOPT_FOLLOWLOCATION => true,
	  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	  CURLOPT_CUSTOMREQUEST => 'GET',
	));

	$response = curl_exec($curl);

	curl_close($curl);

	//print_r($response); 
	$carrera  = json_decode($response, true);
		
	$html = "";
	$html.= '<option value="">SELECCIONE CARRERA</option>';	
	foreach ($carrera['items'] as $k=>$row) {				
		$html.='<option value="'.$row['codigoCarrera'].'">'.$row['nombreCarrera'].'</option>';				
	}
	$html.= '<option value="0"> OTRA CARRERA </option>';
	print $html;
} else {
	print '<option value="0"> CARRERA NO ENCONTRADA </option>';
}