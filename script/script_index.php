<script type="text/javascript">
    $(document).ready(function() {

        //Oculto panel de carrera de origen para mostrar cuando no poseea equivalencias    
        $('.des_origen_ok').hide();
        
        //Oculto panel de carrera de origen para mostrar cuando no poseea equivalencias    
        $('.des_no_encuentra').hide();


        //Oculto contenedor en rojo
        $('.frm_admision').hide();
       
        $('.js-example-basic-single').select2();
       
        /*-----------------------------------------------------------
            Desabilito el boton consultar
        ------------------------------------------------------------*/

        $("#btn-consultar").prop('disabled', true);        
        //$('.hide_btn_consultar').hide();

        /*-----------------------------------------------------------
            Si cambia lista1 -> devuelve datos a lista2
        ----------------------------------------------------------- */
        $('#lista1').change(function() {            
            $.ajax({
                type: "POST",
                url: "script/ajax_mododest.php",
                data: "id_carrera=" + $('#lista1').val(),
                success: function(r) {
                    $('#lista2').html(r);
                }
            });

            //Toma el valor text del option seleccionado.
            let carrera_d = $('#lista1 option:selected').html();
        });

        /*-----------------------------------------------------------
            Lista Modo de Carrera Destino
        -------------------------------------------------------------*/

        $('#lista2').change(function() {

            /* Obtengo el valor del modo */            
            let carrera_d = $('#lista1 option:selected').val();
            let carrera_nombre_d = $('#lista1 option:selected').html();
            let modo_d = $('#lista2 option:selected').val();

            console.log('Carrera: ',carrera_d);
            console.log('Carrera Nombre: ',carrera_nombre_d);
            console.log('Modo: ',modo_d);            
            
            /* Si el modo es 9 es que no tiene MODO definido o no tiene equivalencias   */
            if ((modo_d == 9) || (carrera_d == 9)) {
                //Oculta el panel de la derecha de carreras origen.    
                $('.des_origen').hide();

                //Muestro panel con formulario de consulta.    
                $('.des_no_encuentra').show();                

                //Oculta boton consultar 
                $('.hide_btn_consultar').hide();

                //Muestra panel rojo
                //$('.frm_admision').show();
                
                $.ajax({
                    type: "POST",
                    url: "php/validar_email.php",                    
                    data: {
                        "carrera_d": $('#lista1 option:selected').html(),
                        "codigo_carrera": $('#lista1 option:selected').val(),
                        "modo_d":$('#lista2 option:selected').html()
                    },
                    success: function(r) {
                        $('#form-info').html(r);
                    }
                });
                

            } else {     
                        
                $.ajax({
                    type: "POST",
                    url: "script/ajax_tipo.php",
                    data: {
                        "carrera_d": $('#lista1').val(),
                        "modo_d": $('#lista2').val()
                    },
                    success: function(r) {
                        $('#lista4').html(r);
                    }
                });
                let modo_d = $('#lista2 option:selected').html();
            }
        });

        /* Select Institución de Origen   */

        $('#lista4').change(function() {
            
            let tipo_inst_d = $('#lista4 option:selected').val();
           
            console.log('Tipo de Inst: ', tipo_inst_d);

            if (tipo_inst_d == 0) {
                /* Si no tiene equiv muestra formulario solicitar info -- script/ajax_mas_info_con_equi.php*/
                
                //Oculta boton consultar 
                $('.hide_btn_consultar').hide();

                //Muestra panel rojo
                $('.frm_admision').show();

                $.ajax({
                    type: "POST",
                    url: "php/validar_email.php",
                    data: {
                        "carrera_d": $('#lista1 option:selected').html(),
                        "codigo_carrera": $('#lista1 option:selected').val(),
                        "modo_d":$('#lista2 option:selected').html()
                    },
                    success: function(r) {
                        $('#form-info').html(r);//$('#formulario-info').html(r)
                    }
                });

            } else {
                /* Si tiene equiv */
                $.ajax({
                    type: "POST",
                    url: "script/ajax_institucion_origen.php",
                    data: {
                        "id_carrera": $('#lista1').val(),                    
                        "modo_dest": $('#lista2').val(),
                        "tipo_inst": $('#lista4').val()
                    },
                    success: function(r) {
                        $('#lista5').html(r);
                    }
                });
            }
            let tipo_o = $('#lista4 option:selected').html();
        });

        $('#lista5').change(function() {

            let inst_d = $('#lista5 option:selected').val();
            
            console.log('Codigo Institución: ', inst_d);

            /*****************************************************************
                Si Institucion Origen es UCASAL entonces redirigir al SAG
            ******************************************************************/
           
            if(inst_d == 44001){
                
                //Oculta boton consultar 
                $('.hide_btn_consultar').hide();

                $("#lista6").prop('disabled', true);
                $("#lista7").prop('disabled', true);

                //alert("Las equivalencias internas se tramitan por Sistema de Autogestión (SAG)");                
                $.ajax({
                    type: "POST",
                    url: "mensaje_sag.php",                    
                    success: function(r) {
                        $('#form-info').html(r);
                    }
                });
                
            }//end if
           

            /*---------------------------------------------------------------------- */            
            if (inst_d == 9) {
                //Oculta boton consultar 
                $('.hide_btn_consultar').hide();

                //Muestra panel rojo
                //$('.frm_admision').show();
                
                $.ajax({
                    type: "POST",
                    url: "php/validar_email.php",
                    data: {                        
                        "carrera_d": $('#lista1 option:selected').html(),
                        "codigo_carrera": $('#lista1 option:selected').val(),
                        "modo_d":$('#lista2 option:selected').html()
                    },
                    success: function(r) {
                        $('#form-info').html(r);
                    }
                });

            } else {
                $.ajax({
                    type: "POST",
                    url: "script/ajax_carrera_origen.php",
                    data: {                    
                        "id_carrera": $('#lista1').val(),
                        "modo_dest": $('#lista2').val(),
                        "tipo_inst": $('#lista4').val(),
                        "inst_origen": $('#lista5').val()
                    },
                    success: function(r) {
                        $('#lista6').html(r);
                    }
                });
            }           

            let inst_o = $('#lista5 option:selected').html();
        });

        $('#lista6').change(function() {            
           
            let carrera_origen = $('#lista6 option:selected').val();
            let carrea_nombre_o = $('#lista6 option:selected').html();
            console.log('Carrera Nombre Origen: ',carrea_nombre_o);

            if (carrera_origen == 0) {     
                //Escondo boton consulta 
                $('.hide_btn_consultar').hide();
                
                //Muestra panel rojo
                //$('.frm_admision').show();

                $.ajax({
                    type: "POST",
                    url: "php/validar_email.php",
                    data: {                        
                        "carrera_d": $('#lista1 option:selected').html(),
                        "codigo_carrera": $('#lista1 option:selected').val(),
                        "modo_d":$('#lista2 option:selected').html(),
                        "carrera_nombre_o":$('#lista6 option:selected').html()
                    },
                    success: function(r) {
                        $('#form-info').html(r);
                    }
                });

            } else {

                $.ajax({
                    type: "POST",
                    url: "script/ajax_plan_estudio.php",
                    data: {                    
                        "id_carrera": $('#lista1').val(),
                        "modo_dest": $('#lista2').val(),
                        "tipo_inst": $('#lista4').val(),
                        "inst_origen": $('#lista5').val(),
                        "id_carrera_origen": $('#lista6').val(),
                        "carrera_nombre_o":$('#lista6 option:selected').html()
                    },
                    success: function(r) {
                        $('#lista7').html(r);
                    }
                });
            }
            let carrera_o = $('#lista6 option:selected').html();
        });

        $('#lista7').change(function() {

            let plan_origen = $('#lista7 option:selected').val();

            if(plan_origen == 0){
                
                $('.hide_btn_consultar').hide();

                //Muestra panel rojo
                //$('.frm_admision').show();
                
                $.ajax({
                    type: "POST",
                    url: "php/validar_email.php",                    
                    data: {                        
                        "carrera_d": $('#lista1 option:selected').html(),
                        "codigo_carrera": $('#lista1 option:selected').val(),
                        "modo_d":$('#lista2 option:selected').html()
                    },
                    success: function(r) {
                        $('#form-info').html(r);
                    }
                });

            } else {
                
                $("#btn-consultar").prop('disabled', false);
                $('.hide_btn_consultar').hide();
                
                //Muestro panel rojo.
                $('.frm_admision').show();

                let tipo_o = $('#lista4 option:selected').html();
                let inst_o = $('#lista5 option:selected').html();
                let carrera_o = $('#lista6 option:selected').html();
                let plan_o = $('#lista7 option:selected').html();                       

                if (
                    tipo_o == "" ||
                    inst_o == "" ||
                    carrera_o == "" ||
                    plan_o == ""
                ) {
                    //--------------------------------------------------------------------
                    //--- Si no encuentra datos ------------------------------------------
                    // cambia el texto del botón a MAS INFO
                    //--------------------------------------------------------------------                                    
                   
                    //Muestra panel rojo
                    //$('.frm_admision').show();

                    /*--- Si encuentra ------------------------------*/
                    $.ajax({
                        type: "POST",
                        //url: "script/ajax_mas_info.php",
                        url: "php/validar_email.php",
                        data: {                        
                            "carrera_d": $('#lista1 option:selected').html(),
                            "modo_d": $('#lista2 option:selected').html()
                        },
                        success: function(r) {
                            $('#form-info').html(r);
                        }
                    });

                } else {
                  
                    /*--- Si no encuentra ------------------------------*/
                    $('.hide_btn_consultar').hide();
                
                    $.ajax({
                        type: "POST",
                        url: "validar_email_con_equivalencias.php",
                        data: {                        
                            "id_carrera": $('#lista1').val(),
                            "modo_dest": $('#lista2').val(),
                            "tipo_inst": $('#lista4').val(),
                            "inst_origen": $('#lista5').val(),
                            "id_carrera_origen": $('#lista6').val(),
                            "plan_estudio": $('#lista7').val()
                        },
                        success: function(r) {
                            $('#formulario-info').html(r);                            
                        }
                    });

                }//end if

            }//end if
            
        });
      
        var objeto_window_referencia;
        var configuracion_ventana = "menubar=yes,location=yes,resizable=yes,scrollbars=yes,status=yes";

        function abrir_Popup() {
            objeto_window_referencia = window.open("php/validar_email.php", "Ingresar Datos", configuracion_ventana);
        }     

    }) //End Funcion Main
</script>