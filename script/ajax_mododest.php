<?php
session_start();
include('../php/config.php');

if (isset($_POST['id_carrera'])) {
	
	$codigoCarrera = $_POST['id_carrera'];	
	
	/***********************************************************
	 Obtiene el Sector a partir de la carrera
	************************************************************/
	include('endpoints/recupera-sector.php');
	
	$sector = json_decode($response, true);
		
	$sectorCodigo = 0;

	foreach ($sector as $row) {		
		$sectorCodigo = $row['codigo'];		
		break;
	}//end foreach
	
	/**************************************************************************
	Si sector es 15 (Subsede BS AS) entonces reenvio a Ciencias Juridicas Sector 3
	****************************************************************************/
	if($sectorCodigo == 15){
		$sectorCodigo = 3;
	}
		
	//Envio por session el codigo sector
	$_SESSION['codigoSector'] = $sectorCodigo;	

	
	/*************************************************
	 		Listar modo de la carrera - JAVA 
	**************************************************/
	$curl = curl_init();

	curl_setopt_array($curl, array(
		CURLOPT_URL => 'http://'.$html_link.'/equivalencias/carrera-modos?carrera='.$codigoCarrera,
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => '',
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 0,
		CURLOPT_FOLLOWLOCATION => true,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => 'GET',
	));

	$response = curl_exec($curl);

	curl_close($curl);

	$modo = json_decode($response, true);
	
	$html = '';	
	$html .= '<option value="9">SELECCIONE MODO</option>';

	$una_vez = 0; //controla que no duplique a distancia.	
	$control = 0; //si permanece en cero muestra rayitas  --.--
	
	foreach ($modo as $row) {				
		if($row['codigo'] == 1 ) {
			$html .= '<option value="1">PRESENCIAL</option>';			
		} //end if
		
		if ($una_vez == 0) {
			if(($row['codigo'] == 2) || ($row['codigo'] == 5) || ($row['codigo'] == 7) || ($row['codigo'] == 0)){
				$html .= '<option value="0">DISTANCIA</option>';
				$una_vez = 1;
			} //end if
		}//end if

		$control = 1;

	}//end foreach

}//if POST

if($control == 0){
	$html .= '<option value="9"> MODO NO ENCONTRADO </option>';
}

print $html;