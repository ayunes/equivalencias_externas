<?php
session_start();

include('../php/config.php');  

if (isset($_POST['modo_d'])) {    
    $nombre_carrera = $_POST['carrera_d'];    
    $nombre_modo    = $_POST['modo_d'];
    $codigo_carrera = $_POST['codigo_carrera']; 
    $codigo_sector  = $_SESSION['codigoSector'];    
    $nombre         = $_POST['nombre'];     
    $email          = $_POST['email'];     
    $n_celular      = $_POST['n_celular'];     

    if($n_celular == ''){
        $n_celular = 0;
    }
}

/*
    Enviar datos a POSTULANTES
*/

$curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_URL => 'https://'.$srv_email.'.ucasal.edu.ar/postulantes_mail1.php',
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => '',
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 0,
  CURLOPT_FOLLOWLOCATION => true,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => 'POST',
  CURLOPT_POSTFIELDS => 'id_origen=480&cbx_provincia=16&cbx_sede=1&email='.$email.'&modo=1&nombre='.$nombre.'&tipo_tel=celular&origen=portal-equivalencias-externas&cbx_carrera='.$codigo_carrera.'&cod_area=0&tel='.$n_celular.'',
  CURLOPT_HTTPHEADER => array(
    'Content-Type: application/x-www-form-urlencoded',
    'Cookie: ADF_WEB=es9lrplr0ai95ltljon42nhs51'
  ),
));

$response = curl_exec($curl);

curl_close($curl);
?>

<a name="solicitar_info"></a>

<div class="contenedor_titulo_formulario">
    <span class="titulo_formulario"> Solicitar Información </span> <br> 
    <span class="text_titulo"> Complete el siguiente formulario para recibir mas información sobre equivalencias. </span> 
</div>
    
<div class="form_selector">
    <div class="container">    
        <br/>
        <?php   include('../php/inputs_formulario_mas_info.php');  ?>    
    </div>
</div><!-- end card-body -->

<script type="text/javascript">
    
    $(document).ready(function() {
        $(location).attr('href','#solicitar_info');   

        //$(document).ready(function() {
            setTimeout(function() {
                $("#apellido").focus();
            }, 500);
        //});
    });
    
</script>