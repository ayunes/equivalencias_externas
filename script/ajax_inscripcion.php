<?php
session_start();

if (isset($_POST['modo_d'])) {    
    $nombre_carrera = $_POST['carrera_d'];    
    $nombre_modo    = $_POST['modo_d'];
    $codigo_carrera = $_POST['codigo_carrera'];   
    $nombre         = $_SESSION['nombre'];
    $email          = $_SESSION['email'];  

    //Sector
    $codigo_sector  = $_SESSION['codigoSector'];
}

?>

<a name="solicitar_info"></a>

    <div class="contenedor_titulo_formulario">
        <span class="titulo_formulario"> INSCRIPCIÓN </span> <br/>         
        <span class="text_titulo"> Para iniciar el tramite de inscripcion por favor complete los siguientes datos. </span> 
    </div>

    <div class="form_selector">
        <div class="card-footer">
            <div class="col-auto text-center titulo-carrera"><b>DATOS DEL ALUMNO</b></div>
        </div>

        <div class="card-body">
            
        <?php
            include('../php/inputs_formulario_inscripcion.php');
        ?>    

        </div><!-- end card-body -->

    </div><!-- end card -->

 <script type="text/javascript">    
    $(document).ready(function() {
        //$(location).attr('href','#solicitar_info');
        $(location).attr('href','#marca-formulario-insc');
        

        setTimeout(function() {
            $("#apellido").focus();
        }, 500);
    });

</script>