 <article>
     <div class="card border-secondary">

         <div class="card-header bg-light">
             <div class="col-auto text-center titulo-carrera"><b>CARRERA DE DESTINO</b></div>
             Seleccione la carrera en la que desea inscribirse.
         </div>

         <?php
            /*-------------------------------------------
                Listado de carreras de grado - ORDS
            --------------------------------------------*/

            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => 'https://sistemas.ucasal.edu.ar/'.$srv.'/web/equivalencias-externas/carreras_grado/',
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'GET',
            ));

            $response = curl_exec($curl);

            curl_close($curl);

            $carreraGrado = json_decode($response, true);

            ?>

         <div class="card-body">
             <div class="mb-3">
                 <select class="lGanteCon form-select" aria-label="Ingrese la Facultad" name="carrera_dest" id="lista1" required="required" autofocus="autofocus">
                     <option value="">SELECCIONE CARRERA</option>
                     <?php
                        foreach ($carreraGrado['items'] as $key => $row) {      
                            //if($row['codigocarreragrado'] != 190){
                                print '<option value="' . $row['codigocarreragrado'] . '">' . $row['nombrecarreragrado'] . '</option>';
                            //}
                        }
                        ?>
                     print '<option value="9"></option>';
                 </select>
             </div><!-- mb3 -->
             
             <!-- <div id="lista2"></div> -->             
             <div class="mb-3">
                <!--<div  id="lista2" ></div>-->
                 <select class="lGanteSin form-select" aria-label="Modo" name="modo_dest" id="lista2" required="require">
                     <option value="">SELECCIONE MODO</option>
                 </select>
             </div>
             

         </div><!-- card-body -->
     </div><!-- card -->
 </article>
 <br>