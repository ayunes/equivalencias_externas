const fs = require('fs');

fs.writeFile('./archivo01.txt','Linea 1 \n Línea 2',error => {
    if (error)
        console.log(error);
    else 
        console.log('El archivo fue creado');        

});

console.log('Ultima línea del programa');


fs.readFile('./archivo01.txt', (error,datos) => { 
    if(error)
        console.log(error);
    else    
        console.log(datos.toString());
});

console.log('Ultima línea del programa');