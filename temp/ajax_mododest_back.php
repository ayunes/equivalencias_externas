<?php
session_start();
include('../php/config.php');

if (isset($_POST['id_carrera'])) {
	
	$codigoCarrera = $_POST['id_carrera'];	

	/* ORDS */
	$curl = curl_init();

	curl_setopt_array($curl, array(
	  CURLOPT_URL => 'https://sistemas.ucasal.edu.ar/'.$srv.'/web/equivalencias-externas/codigoSectorDestino/'.$codigoCarrera.'',
	  CURLOPT_RETURNTRANSFER => true,
	  CURLOPT_ENCODING => '',
	  CURLOPT_MAXREDIRS => 10,
	  CURLOPT_TIMEOUT => 0,
	  CURLOPT_FOLLOWLOCATION => true,
	  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	  CURLOPT_CUSTOMREQUEST => 'GET',
	));

	$response = curl_exec($curl);

	curl_close($curl);
		
	$sector = json_decode($response, true);
		
	foreach ($sector['items'] as $k => $row) {		
		print 'Codigo Sector: '.$codigoSector = $row['codigosectordestino'];
		break;
	}

	$html = "";
	$html .= '<option value="9">SELECCIONE MODO</option>';

	if (!isset($codigoSector)) {

		$html .= '<option value="9">MODO SIN DEFINIR</option>';

	} else {

		/*  Creo la $_SESSION['codigoSector'] para utilizarla en los otros selects anidados 	*/
		$_SESSION['codigoSector'] = $codigoSector;
		
		/* 
			ORDS - Consulto si existe tipo de institución para la carrera 
			Si no existe derivo directamente al formulario
		*/

		
		$curl = curl_init();

		//CURLOPT_URL => 'https://sistemas.ucasal.edu.ar/'.$srv.'/web/equivalencias-externas/tipoinstitucion/'.$codigoSector.'/'.$codigoCarrera.'/0',

		curl_setopt_array($curl, array(
		  CURLOPT_URL => 'https://sistemas.ucasal.edu.ar/'.$srv.'/web/equivalencias-externas/tipoinstitucion/'.$codigoCarrera,
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => '',
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 0,
		  CURLOPT_FOLLOWLOCATION => true,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => 'GET',
		));
	
		$response = curl_exec($curl);	
		curl_close($curl);
		$tipo = json_decode($response, true);	
	
		$c = 0;
		foreach ($tipo['items'] as $k=>$row) {		
		  $c = $c + 1;
		}
		
		/*  ORDS - Carrera Sector --------------------------------------------------------------- */
		//CURLOPT_URL => 'https://sistemas.ucasal.edu.ar/'.$srv.'/web/equivalencias-externas/carrerasector/'.$codigoCarrera.'/'.$codigoSector.'',

		print '<br>';
		print $codigoCarrera;
		print '<br>';
		print $codigoSector;

		/*
		$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_URL => 'https://sistemas.ucasal.edu.ar/'.$srv.'/web/equivalencias-externas/carrerasector/'.$codigoCarrera.'/'.$codigoSector,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'GET',
		));

		$response = curl_exec($curl);

		curl_close($curl);
		*/
		$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_URL => 'https://sistemas.ucasal.edu.ar/desa/web/equivalencias-externas/carrerasector/'.$codigoCarrera.'/'.$codigoSector,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'GET',
		));

		$response = curl_exec($curl);

		curl_close($curl);
		echo $response;
			
		$modo = json_decode($response, true);				
		
		/* Si tiene intitucion listo los modos completos   */		

		// Controlar que el modo a distancia/presencial solo aparezca una vez
		$distancia = 0; 
		$presencial = 0;
		$c = 1;
		$una_vez = 0;
		if ($c > 0){
			foreach ($modo['items'] as $k => $row) {		


				if($row['modo'] == 1 ) {
					$html .= '<option value="1">PRESENCIAL</option>';						
				} 

				if(($row['modo'] != 1) && ($una_vez == 0)){
					$html .= '<option value="0">DISTANCIA</option>';
					$una_vez = 1;						
				} 
				
				//---Selecciono y clasifico los modos -------------------------------
				/*
				if(($row['modo'] != 1  )&&($distancia == 0)) {
					$html .= '<option value="2">DISTANCIA</option>';	
					$distancia = 1;
				} 
				
				if(($row['modo'] == 1)&&( $presencial == 0)) {
					$html .= '<option value="1"> PRESENCIAL</option>';	
					$presencial = 1;
				}
				*/				
				//$html .= '<option value="' . $row['modo'] . '">' . $row['nombre'] . '</option>';
			}
		} else {
			/* Si no tiene intitucion listo los modos pero derivo a formulario por eso value='9'   */		
			//foreach ($modo['items'] as $k => $row) {				
			//	$html .= '<option value="9">' . $row['nombre'] . '</option>';
			//}
		}//end if
	}

	/*	Devuelvo los datos generados al div lista2 en carrera_destino.php	*/

}//if control de modo
	
print $html;