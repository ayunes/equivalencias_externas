<?php
session_start();
if (isset($_POST['id_carrera'])) {

    include('../php/config.php');
        
    $id_sector      = $_SESSION['codigoSector'];
    $id_carrera     = $_POST['id_carrera'];
    $modo_dest      = $_POST['modo_dest'];
    $tipo_inst      = $_POST['tipo_inst'];    
    $inst_origen    = $_POST['inst_origen'];

    
    print 'Sector: '.$id_sector;
    print '<br>';
    print 'Carrera: '.$id_carrera;
    print '<br>';
    print 'Modo: '.$modo_dest;
    print '<br>';
    print 'Tipo Inst: '.$tipo_inst;    
    print '<br>';
    print 'Institución: '.$inst_origen;
    print '<br>';
   
    /* API - ORDS */
    $curl = curl_init();
    
    curl_setopt_array($curl, array(
        CURLOPT_URL => 'https://sistemas.ucasal.edu.ar/'.$srv.'/web/equivalencias-externas/materias/'.$id_carrera.'/'.$inst_origen,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'GET',
        CURLOPT_HTTPHEADER => array(
            'Cookie: SAGSERVID=SAGSERVID_1'
        ),
    ));
    
    $response = curl_exec($curl);

    curl_close($curl);    

    //----------------------------------------------------------------------------------------------------------------
    $carrera = json_decode($response, true);
    $html = "";
    $html .= '<option value="0">SELECCIONE CARRERA ORIGEN</option>';
    
    foreach ($carrera['items'] as $k => $row) {            
        $html .= '<option value="' . $row['codigocarreraorigen'] . '">' . strtoupper($row['nombrecarreraorigen']) . '</option>';    
    }    
    print $html;
} else {
    print '<option value="0">CARRERA NO LISTADA</option>';
}//END IF
