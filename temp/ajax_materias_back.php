<?php
session_start();

include('../php/config.php');

$id_sector          = $_SESSION['codigoSector'];
$id_carrera         = $_POST['id_carrera'];
$modo_dest          = $_POST['modo_dest'];
$tipo_inst          = $_POST['tipo_inst'];
$inst_origen        = $_POST['inst_origen'];
$id_carrera_origen  = $_POST['id_carrera_origen'];
$plan_estudio       = urlencode($_POST['plan_estudio']);
$nombre             = $_POST['nombre'];
$email              = $_POST['email'];
$n_celular          = $_POST['n_celular'];

if($n_celular == ''){
    $n_celular = 0;
}

/* armo session para pasar datos a multiples archivos */
$_SESSION['id_carrera']         = $_POST['id_carrera'];
$_SESSION['modo_dest']          = $_POST['modo_dest'];
$_SESSION['tipo_inst']          = $_POST['tipo_inst'];
$_SESSION['inst_origen']        = $_POST['inst_origen'];
$_SESSION['id_carrera_origen']  = $_POST['id_carrera_origen'];
$_SESSION['plan_estudio']       = $_POST['plan_estudio'];
$_SESSION['nombre']             = $_POST['nombre'];
$_SESSION['email']              = $_POST['email'];
$_SESSION['n_celular']          = $_POST['n_celular'];

/*
    Enviar datos a POSTULANTES
*/
$curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_URL => 'https://www-desa.ucasal.edu.ar/postulantes_mail1.php',
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => '',
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 0,
  CURLOPT_FOLLOWLOCATION => true,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => 'POST',
  CURLOPT_POSTFIELDS => 'id_origen=480&cbx_provincia=16&cbx_sede=1&email='.$email.'&modo=1&nombre='.$nombre.'&tipo_tel=celular&origen=portal-equivalencias-externas&cbx_carrera='.$id_carrera.'&cod_area=0&tel='.$n_celular.'',
  CURLOPT_HTTPHEADER => array(
    'Content-Type: application/x-www-form-urlencoded',
    'Cookie: ADF_WEB=es9lrplr0ai95ltljon42nhs51'
  ),
));

$response = curl_exec($curl);

curl_close($curl);

//--- Obtengo listado de materias equivalentes ---------------------------------------

$curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_URL => 'https://sistemas.ucasal.edu.ar/'.$srv.'/web/equivalencias-externas/listadomateriaspar/?p1_e_carrera='.$id_carrera_origen.'&p1_e_plan='.$plan_estudio.'&p1_carrera='.$id_carrera,
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => '',
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 0,
  CURLOPT_FOLLOWLOCATION => true,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => 'GET',
));

$response = curl_exec($curl);

curl_close($curl);

$materias = json_decode($response, true);

?>
<br />


<div class="contenedor_titulo_formulario">
    <span class="titulo_formulario">DETALLE DE EQUIVALENCIAS OTORGADAS</span>  
    <br>
    <span class="text_titulo">A continuación se muestran antecedentes registrados al día de la fecha en UCASAL, según su institución, carrera y plan de origen.</span>
</div><!-- contenedor_titulo_formulario -->


<table class='table table-hover table-striped'>
    <thead style="background-color:blanck; ">
        <tr>
            <th class="titulo-equiv">MATERIAS UNIVERSIDAD DE ORIGEN</th>
            <th class="titulo-equiv">MATERIAS DE UCASAL</th>
            <th class="titulo-equiv">CONDICIÓN</th>
        </tr>
    </thead>
    <tbody style="background-color:blanck; ">
        <?php
        if ($materias != null) {
            foreach ($materias['items'] as $k => $row) {
                print '<tr>';
                print '<td>' . $row['materia_universidad_origen'] . '</td>';
                print '<td>' . $row['materia_ucasal'] . '</td>';
                print '<td>' . strtoupper($row['causa']) . '</td>';
                print '</tr>';
            }//end foreach
        } else {
            print '<tr>';
            print '<th colspan="3"> No se encontraron materias equivalentes. </th>';          
            print '</tr>';
        } //end if
        ?>
    </tbody>
</table>

<br/>
<?php 
    include('../php/mensaje.php');      
?>
<br/>

<div class="boton_imprimir">    
    <a  href="equivalencias_pdf.php?id_sector=<?=$id_sector; ?>&id_carrera=<?=$id_carrera;?>&modo_dest=<?=$modo_dest; ?>&tipo_inst=<?=$tipo_inst;?>&inst_origen=<?=$inst_origen; ?>&id_carrera_origen=<?=$id_carrera_origen; ?>&plan_estudio=<?=$plan_estudio; ?>"   
        class="btn btn-outline-secondary" id="btn-imprimir" target="_blank" >
        IMPRIMIR
    </a>
</div>
<div class="text-center">   
<br>
    <div class="row">
        <div class="d-grid gap-2 col-8 mx-auto">
            <button class="btn btn-primary btn-sm" id="btn-inscripcion">ABONAR MATRÍCULA</button>
        </div>
        <div class="d-grid gap-2 col-4 mx-auto">
            <a href="#solicitar_info" class="btn btn-secondary btn-sm" id="btn-info2">SOLICITAR MAS INFORMACIÓN</a>                
        </div>
    </div><!-- row -->
</div><!-- text center -->
<hr />
<br>
<script type="text/javascript">
    $("#btn-info2").click(function() {
        $.ajax({
            type: "POST",
            url: "script/ajax_mas_info_con_equi.php",
            data: {                
                "carrera_d": $('#lista1 option:selected').html(),
                "modo_d": $('#lista2 option:selected').html(),
                "codigo_carrera": $('#lista1 option:selected').val()               
            },
            success: function(r) {
                $('#formulario-insc').html(r);
            }
        });
    });

    $("#btn-inscripcion").click(function() {
        $.ajax({
            type: "POST",
            url: "script/ajax_inscripcion.php",
            data: {                
                "carrera_d": $('#lista1 option:selected').html(),
                "modo_d": $('#lista2 option:selected').html(),
                "codigo_carrera": $('#lista1 option:selected').val()
            },
            success: function(r) {
                $('#formulario-insc').html(r);
            }
        }); 
    });
    
</script>

<script type="text/javascript">
    /*---------------------------------------------------------
        POSICIONAMIENTO LINK INTERNO
    ---------------------------------------------------------*/
    $(document).ready(function() {
        $(location).attr('href','#listar_materias');
       
        //Oculto mensaje en footer
        $('.mostrar_mensaje').hide();        
        
    });
</script>