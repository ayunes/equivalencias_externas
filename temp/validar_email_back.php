<?php
if (isset($_POST['carrera_nombre_o'])) {    
    $_SESSION['carrera_nombre_o'] = $_POST['carrera_nombre_o'];
}
?>

<br>
<div class="contenedor_formulario">
    
    <div class="contenedor_titulo_formulario_red">
        <span class="titulo_formulario"> No se encotraron equivalencias en la carrera solicitada, ingrese los siguientes datos para continuar. </span> 
    </div>

    <div class="form_admision">

        <div class="card-body">

            <div class="mb-3">
                <input type="text" class="form-control" id="nombre" name='nombre' value="" placeholder="Nombre" required=" required"> 
            </div>     
            
            <div class="mb-3">
                <input type="email" class="form-control" id="email" name='email' value="" placeholder="Email" required>                
            </div>

            <div class="mb-3">
                <input type="text" class="form-control" id="n_celular" name='n_celular' placeholder="Nº teléfono(opcional)" >
            </div>

            <div class="text-right">
                <p style="color: grey;">* Un representante de la universidad se pondrá en contacto a través de las vías proporcionadas.</p>
            </div>

            <div class="text-center">        
                <button class="btn btn-danger" id="btn-numero3-info"  type="submit" name="enviar">CONTINUAR</button>
            </div>

        </div><!-- card-body -->

    </div> <!-- form_selector --> 
    
</div><!-- contenedor formulario -->

<br>

<?php
    include('php/foot.php');    
?>
<script>
    $(document).ready(function() {        
        $(location).attr('href','#solicitar_info'); 

        setTimeout(function() {
            $("#nombre").focus();
        }, 500);
             
    });
   
    $("#btn-numero3-info").on("click", function() {       
        
        if ($("#nombre").val().length < 1 || $("#email").val().length < 1) {
            
            alert("Ingrese nombre y mail para continuar");
            
            setTimeout(function() {
                $("#nombre").focus();
            }, 500);

        } else {
            $.ajax({
                type: "POST",
                url: "script/ajax_sin_equivalencias.php",
                data: {                        
                    "carrera_d": $('#lista1 option:selected').html(),
                    "codigo_carrera": $('#lista1 option:selected').val(),
                    "modo_d":$('#lista2 option:selected').html(),
                    "nombre":$('#nombre').val(),
                    "email":$('#email').val(),
                    "n_celular":$('#n_celular').val()
                },
                success: function(r) {                    
                    $('#listar_materias').html(r);
                    //$('#formulario-info').html(r);
                }
            });      

        }//end if        
       
    }); 
</script>