<?php 
    session_start(); 
    define ('SITE_ROOT', realpath(dirname(__FILE__)));
?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title> Consultas de Equivalencias </title>
        
        <!-- Favicon-->
        <link rel="icon" type="image/x-icon" href="assets/favicon.ico" />
        
        <!-- Core theme CSS (includes Bootstrap)-->
        <link href="css/styles.css" rel="stylesheet" />     

        <script src="js/scripts.js"></script>
        
        <!-- TOAST -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.0.1/css/toastr.css" rel="stylesheet"/>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.0.1/js/toastr.js"></script>
        
        <!-- JQUERY -->
        <!--
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
        <script src="js/jquery3.js"></script>
        --> 
        
        <!-- Select2 Form -->        
        <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />        
        <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script> 
    </head>
    
    <body style="background-color:#f3f3f3;" class="hidden">
        
        <?php
           header ('Cache-Control: no-store, no-cache, must-revalidate, max-age = 0');
           include('php/nav.php');         
           include('php/config.php');
        ?>

    <div class="container">

    <br>
    <div class="col-auto text-center">
        <h3>Su trámite se realizó con éxito </h3>
    </div>
    <br>
    <ul>   
        <li>Se ha enviado un email a su casilla de correo con los datos ingresados.</li>
        <!-- <li>Puede consultar el seguimiento de su expediente desde el siguiente link -><a href="https://www-desa.ucasal.edu.ar/equivalencias_externas//consultar_expediente.php">consultar expediente</a>.</li> -->
    </ul>

<?php

if (isset($_POST['enviar'])) {

     /*  Datos del Alunmo */    
    $nombre             = $_POST['nombre'];
    $apellido           = $_POST['apellido'];
    $t_docu             = $_POST['t_docu'];
    $n_docu             = $_POST['n_docu'];    
    $email_alumno       = $_POST['email'];

    /* Datos del consultante a ingresar en observaciones */
    $datos_estudiante = $nombre.', '.$apellido.' - DNI: '.$n_docu.' - '.$email_alumno;
    
    if(isset($_SESSION['carrera_nombre_o'])){
        $carrera_nombre_o   = $_SESSION['carrera_nombre_o'];
    } else {
        $carrera_nombre_o = 'Sin elegir';
    }    

    /*  Datos de la carrera */
    $nombre_carrera     = $_POST['nombre_carrera'];
    $codigo_carrera     = $_POST['codigo_carrera'];
    $nombre_modo        = $_POST['nombre_modo'];
    $codigo_sector      = $_POST['codigo_sector'];      
    $observaciones      = RemoveSpecialChar($_POST['observaciones']).' - '.$datos_estudiante;
    
    //--- Tipo de Formulario ---------------------------------------
    $tipo_form = $_POST['tipo_form'];

    //---------------------------------------------------------------------        
    /*  Busco Persona - ORDS - parametro: $n_docu */ 
    include('endpoints/bucar_persona.php');        
    $persona = json_decode($response, true);

    $c = 0;
    foreach ($persona['items'] as $k => $row) {
        $c = $c + 1;        
        $id_persona = $row['personaid'];        
    }    
    
    if ($c <= 0) {                           
        /********************************************
                Si no encuentra Persona JSON 
        **********************************************/
        include('endpoints/inserta_persona.php');     
        
        //--- Buscar Persona - ORDS --------------------
        include('endpoints/bucar_persona.php');  

        $persona = json_decode($response, true);
        
        $id_persona = 999;
        
        foreach ($persona['items'] as $k => $row) {        
           $id_persona = $row['personaid'];        
        }

        //--- Insertar Email - ORDS --------------------------------------------------------------        
        include('endpoints/inserta_mail.php');  
      
    } else {        
        //---  Si existe recuperamos ID de la persona ingresada  --------------------------
        
        //--- Buscar Persona - ORDS --------------------------------------------------------    
        include('endpoints/bucar_persona.php');  

        $persona = json_decode($response, true);        

        foreach ($persona['items'] as $k => $row) {            
            $id_persona = $row['personaid'];
        }
        
        //--- Recuperamos MAIL ---------------------------------------------------------------
        include('endpoints/recupera_mail.php');  
        $recuperaMmail = json_decode($response, true);        

        $mail_persona = "";
        foreach ($recuperaMmail as $row) { 
            $mail_persona = $row['mai_MAIL'];  
        }        
        
        //Si encuentra mail guardado
        if(isset($mail_persona)){            
            //Comparo el mail ingresado con el mail guardado en la BD.
            if (strcmp(strtolower($email_alumno), strtolower($mail_persona)) !== 0) {
                
                //Si no son iguales actualizo el mail de la bd por el ingresado                
                $curl = curl_init();

                curl_setopt_array($curl, array(
                    CURLOPT_URL => 'http://'.$html_link.'/equivalencias/update-mail',
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => '',
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 0,
                    CURLOPT_FOLLOWLOCATION => true,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => 'PUT',
                    CURLOPT_POSTFIELDS =>'{
                    "mailMail": "'.$email_alumno.'",
                    "per_id": '.$id_persona.'
                    }',
                    CURLOPT_HTTPHEADER => array(
                        'Content-Type: application/json'
                    ),
                ));

                $response = curl_exec($curl);

                curl_close($curl);
            }//end if
            
        } else {
            
            //---- Si no encuentra mail guardado - Insertar Email - ORDS ---------------------------------------
            include('endpoints/inserta_mail.php');  

        } //end if

    } //end if persona

    
    /**********************************************
    Recuparamos Sector nuevamente por seguridad 
    **********************************************/

    $codigoCarrera = $codigo_carrera;
    include('endpoints/recupera-sector.php'); 
    
    $sector = json_decode($response, true);

	foreach ($sector as $row) {		
		$codigo_sector  = $row['codigo'];		
		break;
	}//end foreach
    


    /**************************************************************************************
    Si no encuentra actor en MEU va a secretaria general antes pasaba 0
    **************************************************************************************/
    $codigo_actor = 172; 

    /* Si el formulario es una inscripción envio a Admisiones  */
    if($tipo_form == 'inscripcion'){
        
        //--- Admisiones para Andrea F. ---------------------------
        $codigo_actor = 752;

    } else {

        //--- OBTENEMOS EL ACTOR (actorDestino) A PARTIR DEL SECTOR --------------------------        
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://sistemas.ucasal.edu.ar/'.$srv.'/web/equivalencias-externas/codigoactor/'.$codigo_sector.'',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        
        $c_sector = json_decode($response, true);

        foreach ($c_sector['items'] as $k => $row) {        
            $codigo_actor = $row['codigoactor'];        
        }    
            
        /* Control si Facultad Económica es modo presencial o a distancia &&($codigo_carrera == 14)   */    
        if(($codigo_sector == 2)&&($nombre_modo=="PRESENCIAL")){
            $codigo_actor = 97;
        } elseif (($codigo_sector == 2)&&($nombre_modo !="PRESENCIAL")) {
            $codigo_actor = 62;
        }    
    }//end if control si formulario es incripcion
       
    /*
    ***************************************************************************************************
        INSERTAR SEG_EXPEDIENTE E INSERTO EXPEDIENTE SCH_SEGUIMIENTO.SEG_EXP_DETALLE y EXP_MOVIMS
    ***************************************************************************************************
        Debemos crear un autorCrea por ahora esta usando el 1 Alumno Equivalencia 
    ***************************************************************************************************
    */ 
    
    /* Si observaciones esta en blanco guardo una - */
    if($observaciones == ''){
        $observaciones = '-';
    }

    //Oservaciones en seg_expedientes
    $observaciones = strval($observaciones);

    //Observaciones en seg_exp_detalle
    $obs_detalle = 'Carrera consulta:'.$nombre_carrera.' - Carrera Origen:'.$carrera_nombre_o;
   
    /* ORDS  Inserta los datos en SEG_EXPEDIENTE, SEG_EXP_MOVIMS y SEG_EXP_DETALLE */
    $curl = curl_init();   

    curl_setopt_array($curl, array(
        CURLOPT_URL => 'https://sistemas.ucasal.edu.ar/'.$srv.'/web/equivalencias-externas/createexpequiv/',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS =>'{
            "perIdRemite": "'.$id_persona.'",
            "obsExpediente": "'.$observaciones.'",
            "actorDestino": '.$codigo_actor.',
            "obsDetalle": "'.$obs_detalle.'"
        }',
            CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json'
            ),
    ));
    
    $response = curl_exec($curl);    
    
    curl_close($curl);
    
    $expediente = json_decode($response, true);

    $n_expediente = $expediente['idexp']; 
        
    $anio_expediente = date("Y");
    
    /*------------------------------------------------------------------------        
         INSERTAR ARCHIVO CERTIFICADO ANALITICO
    -------------------------------------------------------------------------*/
    $mes = date("m");

    if  (
            ($_FILES['certificado']['name'] =='') ||
            ($_FILES['certificado']['type'] != 'application/pdf') ||
            ($_FILES['certificado']['size'] > 15728640)
        )
    {
        
        $archivo_certificado = '-';    
        $tipo_certificado = '-';
        $tam_certificado = 0;
        $data_certificado = '-';      

    } else {

        $archivo_certificado = $_FILES['certificado']['name'];
        $tipo_certificado = $_FILES['certificado']['type'];
        $tam_certificado = $_FILES['certificado']['size'];
        $certificado_tmp = $_FILES['certificado']['tmp_name'];

        $archivo_certificado = $anio_expediente.$n_expediente . '_' . $archivo_certificado;
        $path = '/mnt/mesa_entrada/' . $anio_expediente . '/' . $mes.'/'; //http://wl12-desa.ucasal.edu.ar
       

        //Controlo si existe el directorio, para crearlo.
        $path_control = SITE_ROOT.'/mnt/mesa_entrada/' . $anio_expediente . '/' . $mes.'/';
        
        if (!file_exists($path_control)) {
            //print 'No Existe, creando....';
            mkdir($path_control, 0777, true);
        }    
        
        $path_certificado_archivo = SITE_ROOT.'/mnt/mesa_entrada/' . $anio_expediente . '/' . $mes. '/'.$archivo_certificado;                                      
        //print $path_certificado_archivo = '/mnt/mesa_entrada/' . $anio_expediente . '/' . $mes. '/'.$archivo_certificado;                                      
        
        /* Subir archivo al servidor */
        if (move_uploaded_file($certificado_tmp,$path_certificado_archivo)){
            $data_certificado = 'Certificado subido';

            /* ORDS */
            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => 'https://sistemas.ucasal.edu.ar/'.$srv.'/web/equivalencias-externas/createinsarchivo/',
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_POSTFIELDS =>'{
                    "codExpediente": ' . $n_expediente . ',
                    "docuNombre":"'. $archivo_certificado .'",
                    "docuTam":'.$tam_certificado .',
                    "docuPath":"'.$path.'",
                    "numeroMovim":1
                }',
                CURLOPT_HTTPHEADER => array(
                    'Content-Type: application/json'
                ),
            ));

            $response = curl_exec($curl);
            curl_close($curl);
            $data_certificado = 'Adjunto';

        } else {
            $data_certificado = 'Certificado no subido';
        }//end if
    }
          
    /*-------------------------------------------------------------------        
        SI EXISTE INSERTO ARCHIVO PLAN DE ESTUDIO
    ----------------------------------------------------------------------*/
     /* Plan de estudio */
     
     if (
            ($_FILES['plan']['name']=='') ||
            ($_FILES['plan']['type'] != 'application/pdf') ||
            ($_FILES['plan']['size'] > 15728640)
        )
    {
        $archivo_plan = '-';
        $tipo_plan = '-';    
        $tam_plan = 0;        
        $data_plan = '-';
     } else {
        $archivo_plan = $_FILES['plan']['name'];
        $tipo_plan = $_FILES['plan']['type'];    
        $tam_plan = $_FILES['plan']['size'];
        $plan_tmp = $_FILES['plan']['tmp_name'];   

        $archivo_plan = $anio_expediente.$n_expediente . '_' . $archivo_plan;             
        
        $path = '/mnt/mesa_entrada/' . $anio_expediente . '/' . $mes.'/';       //. '/'.$archivo_plan http://wl12-desa.ucasal.edu.ar        
                
        $path_plan_archivo = SITE_ROOT.'/mnt/mesa_entrada/' . $anio_expediente . '/' . $mes. '/'.$archivo_plan;   
                        
        /* Subir Archivo al servidor */
        if (move_uploaded_file($plan_tmp,$path_plan_archivo)){
            $data_plan = 'Plan subido';
        
            /* ORDS - Registrar plan de estudio */
            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => 'https://sistemas.ucasal.edu.ar/'.$srv.'/web/equivalencias-externas/createinsarchivo/',
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_POSTFIELDS =>'{
                    "codExpediente": ' . $n_expediente . ',
                    "docuNombre": "' . $archivo_plan . '",
                    "docuTam":' . $tam_plan . ',
                    "docuPath":"' . $path . '",
                    "numeroMovim":1
                }',
                CURLOPT_HTTPHEADER => array(
                    'Content-Type: application/json'
                ),
            ));

            $response = curl_exec($curl);

            curl_close($curl);
            $data_plan = 'Adjunto';
        } else {
            $data_plan = 'Plan no subido';
        }
     }   
    
    /*-------------------------------------------------------------------
        Template Mail Alumno
    -------------------------------------------------------------------*/    
    include('endpoints/template_alumno.php');  

    /*-------------------------------------------------------------------        
         ENVIO EMAIL AL ALUMNO
    ----------------------------------------------------------------------*/        
    include('endpoints/envio_mail_alumno.php');  

    /*--------------------------------------------------------------------                 
        OBTENER MAIL DE SECRETARIO ACADEMICO
    ----------------------------------------------------------------------*/ 
    include('endpoints/obtener_mail_secretario_academico.php');  
    
    $mail_secretario = json_decode($response, true);    
   
    $mail_sec = 'ayunes@ucasal.edu.ar';

    foreach ($mail_secretario['items'] as $k => $row) {        
        $mail_sec = $row['email_institucional'];        
        break;
    }     
    
    //si no encuentra actor envia mail a secretaria general
    if($codigo_actor == 172){        
        $mail_sec = 'salonso@ucasal.edu.ar';
    }
    
    //--- Si es por formulario de inscripción envia admisiones ------
    if( $codigo_actor == 752){        
        $mail_sec = 'apferrer@ucasal.edu.ar';        
    }

    /*-------------------------------------------------------------------
        Template Secretario Academico
    -------------------------------------------------------------------*/
    
    $nombre_usuario = $nombre.' '.$apellido;    

    $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_URL => 'http://template.ucasal.edu.ar/v1/equivalencias/facultad',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS =>'{
            "replaces":{
                "nombre_alumno":"'.$nombre_usuario.'",
                "n_docu":"'.$n_docu.'",
                "nombre_carrera":"'.$nombre_carrera.'",
                "nombre_modo":"'.$nombre_modo.'",
                "n_expediente":"'.$n_expediente.'",
                "anio":"'.$anio_expediente.'",
                "email":"'.$email_alumno.'",
                "observaciones":"'.$observaciones.'"                               
            }
        }',
        CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json'
    ),
    ));
    
    $template_sec_acad = curl_exec($curl);

    //print_r($template_sec_acad);
    
    curl_close($curl);
    
    /*  Envio el email al Secretario Académico   */    

    /****************************************************
        IMPORTANTE CAMBIAR MAIL AUXILIAR PARA PRODUCCION    
    *****************************************************/
    
    $mail_sec_acad = [
        "email"=> [$mail_sec],
        "subject"=>"Equivalencias Externas",
        "template" => $template_sec_acad,
        "replyTo"=>"no-reply@ucasal.edu.ar"    
    ];
        
    $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_URL => 'http://tomeegen1.ucasal.edu.ar:8080/mailbackend/mail/enviar-mail',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS => json_encode($mail_sec_acad),
        CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json'
        ),
    ));

    $response = curl_exec($curl);
    curl_close($curl);
    
    /**********************************************************************************
    //--Email a admision@ucasal.edu.ar igual que a secretario --------------------------
    ********************************************************************************** */
    
    $mail_adm = 'admision@ucasal.edu.ar'; // 
        
    $mail_admision = [
        "email"=> [$mail_adm],
        "subject"=>"Equivalencias Externas",
        "template" => $template_sec_acad,
        "replyTo"=>"no-reply@ucasal.edu.ar"    
    ];

    $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_URL => 'http://tomeegen1.ucasal.edu.ar:8080/mailbackend/mail/enviar-mail',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS => json_encode($mail_admision),
        CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json'
        ),
    ));

    $response = curl_exec($curl);
    curl_close($curl);
    
}//end if post

?>

<!-- <h5>Datos Personales</h5> -->
<table class="table table-strip"> 
    
    <tr>
        <th>Nº Expediente</th>
        <td><?=$n_expediente; ?></td>
    </tr>

    <tr>
        <th>Año</th>        
        <td><?=$anio_expediente; ?></td>
    </tr>
    
    <tr>
        <th>ID Personas </th>
        <td><?=$id_persona; ?></td>
    </tr>    
    
    <tr>
        <th>DNI: </th>
        <td><?=$n_docu; ?></td>
    </tr>

    <tr>
        <th>Nombre</th> 
        <td><?=$nombre; ?></td>        
    </tr>
    <tr>
        <th>Apellido</th>
        <td><?=$apellido; ?></td>
    </tr> 
    <tr>
        <th>Email</th> 
        <td><?=$email_alumno; ?></td>
    </tr>
    
    <tr>
        <th>Carrera</th>
        <td><?=$nombre_carrera; ?></td>        
    </tr>
    <tr>            
        <th>Modo</th>
        <td><?=$nombre_modo; ?></td>
    </tr>
    <!--
    <tr>
        <th>Sector</th>
        <td><?=$codigo_sector; ?></td>
    </tr>
    

    <tr>
        <th>Observaciones SEG_EXP:</th>
        <td><?=$obs_detalle; ?></td>
    </tr>
   
    <tr>
        <th>Codigo Actor</th>
        <td><?=$codigo_actor; ?></td>
    </tr>
    
    <tr>
        <th>Envia mail a: </th> 
        <td><?=$mail_sec; ?></td>
    </tr>
     
    <tr>
        <th>Detalle Carreras</th>
        <td><?= $obs_detalle; ?></td>
    </tr>
    -->
    <tr>
        <th>Observaciones</th>
        <td><?=$observaciones; ?></td>
    </tr>    

    <tr>
        <th>Plan de Estudio</th>
        <td><?=$data_plan; ?></td>
    </tr>

    <tr>
        <th>Certificado analitico</th>
        <td><?=$data_certificado; ?></td>
    </tr>

</table>
<br>

<!--
<table class="table table-strip">
    <thead>
        <tr>
            <th>Archivos Adjuntos</th>
    
            <th>Tipo</th>
            <th>Tamaño</th>
            <th>Ruta</th>
    
            <th>Datos</th>
            <th>Path + Archivo</th>
        </tr>  
    </thead>

    <tbody>    
        <tr>            
            <td><?=$archivo_certificado?></td>
            
            <td>Application/pdf</td>
            <td><?=$tam_certificado; ?></td>
            <td>
                <?php 
                    if(!isset($path_certificado)){
                        print $path_certificado = '-'; 
                    } else {
                        print $path_certificado;
                    }
                ?>
            </td>            
            <td><?=$data_certificado; ?></td>
            <td><?=$path_certificado_archivo; ?></td> 
        </tr>
        <tr>        
            <td><?=$archivo_plan; ?></td>
            
            <td>Application/pdf</td>
            <td><?=$tam_plan; ?></td>
            <td>
                <?php
                    if(!isset($path_plan)){
                        print $path_plan = '-';
                    } else {
                        print $path_plan;
                    }
                ?>
            </td>

            <td><?=$data_plan; ?></td>
            <td><?=$path_plan_archivo; ?></td>        
        </tr>

    </tbody>

</table>
-->

<a href="index.php" class="btn btn-primary">Volver a Inicio </a>
</div><!-- container -->

<?php
    include('php/foot.php');    
    session_unset();
?>

<script type="text/javascript">
 toastr.options = {
            "tapToDismiss": false,
            "closeButton": false,
            "debug": false,
            "newestOnTop": false,
            "progressBar": false,
            "positionClass": "toast-bottom-right",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "30000",
            "hideDuration": "100000",
            "timeOut": "500000",
            "extendedTimeOut": "100000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
         }
toastr.info("<a href='consultar_expediente.php'> Consulte su expediente <b>  aqui</a> </b>")
</script>

<?php
function RemoveSpecialChar($str)
{
    $res =  preg_replace('([^A-Za-z0-9 ])', '', $str);
    return $res;
}

?>