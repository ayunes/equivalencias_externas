<?php
/*-------------------------------------------------------------------
        Template Mail Alumno
-------------------------------------------------------------------*/     
$nombre_usuario = $nombre.' '.$apellido;

$curl = curl_init();

curl_setopt_array($curl, array(
    CURLOPT_URL => 'http://template.ucasal.edu.ar/v1/equivalencias/alumno',
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => '',
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 0,
    CURLOPT_FOLLOWLOCATION => true,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => 'POST',
    CURLOPT_POSTFIELDS =>'{
        "replaces":{
            "nombre_alumno":"'.$nombre_usuario.'",
            "nombre_carrera":"'.$nombre_carrera.'",
            "nombre_modo":"'.$nombre_modo.'",
            "n_expediente":"'.$n_expediente.'",
            "anio":"'.$anio_expediente.'"
        }
    }',
    CURLOPT_HTTPHEADER => array(
        'Content-Type: application/json'
),
));

$template_alumno = curl_exec($curl);

curl_close($curl);

?>