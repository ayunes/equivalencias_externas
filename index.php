<?php
include('php/head.php');
include('php/nav.php');
?>

<div class="container">
    <br>    
    <div class="col-auto text-center">
        <div="titulo_web"> <span class="azul_cat"> >>> </span> CONSULTA DE EQUIVALENCIAS (v2.0) <span class="azul_cat"><<< </span> </div>
        <br/>        
    </div>

    <div class="container-sm">
        <div class="contenedor_titulo_formulario">
            <span class="titulo_formulario"> Carrera de Destino </span> <br> 
            <span class="text_titulo"> Seleccione la carrera en la que desea inscribirse </span> 
        </div>

        <div class="form_consulta">
            <?php include('php/carrera_destino.php'); ?>
        </div>
    </div><!-- container-sm-->

    <br>
    <div class="container-sm">
        <div class="contenedor_titulo_formulario">
            <span class="titulo_formulario"> Carrera de Origen </span>  <br>
            <span class="text_titulo"> Seleccione la carrera de origen </span> 
        </div>

        <div class="form_consulta">
            <?php include('php/carrera_origen.php'); ?>
        </div>
    </div>
    
    <br/>
    <br/>  

    <div class="hide_btn_consultar">
        <div class="col-auto text-center boton-simular">
            <div id="mostrar-consulta">
                <button class="btn btn-outline-secondary" id="btn-consultar" style="background-color: grey; color:#FFF;"> CONSULTAR </button>
            </div>
        </div>
    </div>
       
    <hr/>    
    <div class="link_consultar_expediente">
        <a href="consultar_expediente.php">CONSULTAR EXPEDIENTE</a>
    </div>
    <br/>
    <br/>
</div><!-- container -->


<div class="container" style="background-image: url('https://ucasal.edu.ar/consulta_equiv_externa/media/fondo_formulario.jpg');">
    
    <div class="frm_admision"> <!-- Fondo en rojo -->    
        <a name="solicitar_info"></a>    
        <div id="formulario-info"></div>
    </div><!-- Fondo en rojo -->

</div><!-- container -->

<br>

<div class="container">       
    <a name="form-info"></a>    
    <div id="form-info"></div>

    <a name="listar_materias"></a>    
    <div id="listar_materias"></div>    
    
    <a name="solicitar_inscripcion"></a>
    <div id="formulario-inscripcion"></div>

    <a name="marca-formulario-insc"></a>
    <div id="formulario-insc"></div>    

</div><!-- container -->

<br/>

<div class="container">
    <div class="mostrar_mensaje">
    <?php include('php/mensaje.php');  ?>
    </div>
</div>

<?php
    include('php/foot.php');    
    include('script/script_index.php');
?>

<script type="text/javascript">

//Activa/Desactiva el plugin de select con busqueda 

$(document).ready(
       ()=>{
           $(".lGanteCon").select2();
           $(".lGanteSin").select2({
                minimumResultsForSearch: -1
           });
       }
   )
    
   /*
   toastr.options = {
            "tapToDismiss": false,
            "closeButton": false,
            "debug": false,
            "newestOnTop": false,
            "progressBar": false,
            "positionClass": "toast-bottom-right",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "30000",
            "hideDuration": "100000",
            "timeOut": "500000",
            "extendedTimeOut": "100000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
         }
    toastr.info("<a href='consultar_expediente.php'> Consulte su expediente <b>  aqui</a> </b>")
    */

</script>
