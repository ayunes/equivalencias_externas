<?php
include('php/head.php');
include('php/nav.php');
include('php/control_datos_ingreso.php');
?>
<div class="container">

<?php
if(isset ($_POST['n_dni'])) {
    $n_dni = $_POST['n_dni'];
    $n_exp = $_POST['n_exp'];
    $anio = $_POST['anio'];

    /*------------------------------------------------------------------
        Control Persona
    -------------------------------------------------------------------*/ 

    /* API - ORDS */
    $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_URL => 'https://sistemas.ucasal.edu.ar/'.$srv.'/web/equivalencias-externas/personaid/'.$n_dni.'',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'GET',
    ));

    $response = curl_exec($curl);

    curl_close($curl);

    $persona = json_decode($response, true);
    
    $c = 0;    

    if($persona == null){
        $persona['items'] = 0;
    } else {
        foreach ($persona['items'] as $k => $row) {        
            $c = $c + 1;
        }    
    }    

    if ($c >= 1) {
        
        /*------------------------------------------------------------------
            Estado Expediente
        -------------------------------------------------------------------*/
        /*
            API - ORDS 
        */
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://sistemas.ucasal.edu.ar/'.$srv.'/web/equivalencias-externas/estadoexpediente/'.$n_exp.'/'.$anio.'',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        //echo $response;

        $est_expediente = json_decode($response, true);
        
        $estadoExpedient = "En tramite";
        
        foreach ($est_expediente['items'] as $k => $row) {        
            $estadoExpedient = $row['estadoexpediente'];
            break;
        }
        ?>
            <div class="col-auto text-center"><h4>Datos del Expediente</h4> </div>
            <br/>
            <table class="table table-striped">
                <theader>
                    <tr>
                        <th>N° DNI</th>
                        <th>N° Expediente</th>
                        <th>Año</th>
                        <th>Estado</th>
                    </tr>
                </theader>
                <tbody>
                    <tr>
                        <td><?=$n_dni; ?></td>
                        <td><?=$n_exp; ?></td>
                        <td><?=$anio; ?></td>
                        <td><?=$estadoExpedient; ?></td>
                    </tr>
                </tbody>
            </table>
            <br/>
        <?php

        $c = count($est_expediente);

        if ($c >= 1){

            /*  Movimiento del expediente - API - ORDS */ 

            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => 'https://sistemas.ucasal.edu.ar/'.$srv.'/web/equivalencias-externas/seguimientoexpediente/'.$n_exp.'/'.$anio.'',
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'GET',
            ));

            $response = curl_exec($curl);

            curl_close($curl);            

            $expediente = json_decode($response, true);

            print '<table class="table table-striped">';
            print '<thead>';
            print '<tr>';
            print '<th>Nº</th>';
            print '<th>Remite</th>';
            print '<th>Fecha</th>';            
            print '<th>Recepción</th>';
            print '<th>Fecha Rec.</th>';
            print '<th>Observaciones</th>';                
            print '</tr>';
            print '</thead>';
            print '<tbody>';

            foreach ($expediente['items'] as $k => $row) {
                $movimiento         = $row['numeromovim'];
                $f_crea             = $row['fechacrea'];
                $actor_remite       = $row['actorremite'];
                $actor_destino      = $row['actordestino'];
                $fecha_recepcion    = $row['fecharecep'];
                $observaciones      = $row['observaciones'];

                //--- Arreglo de fecha ------------------------
                $f_crea = substr($f_crea,0,10);
                $d = substr($f_crea,-2);
                $m = substr($f_crea,5,2);
                $a = substr($f_crea,0,4);

                $f_crea_nueva = $d.'/'.$m.'/'.$a;
                //----------------------------------------------
                $f_recepcion = substr($fecha_recepcion,0,10);
                $d = substr($f_recepcion,-2);
                $m = substr($f_recepcion,5,2);
                $a = substr($f_recepcion,0,4);

                $f_recepcion_nueva = $d.'/'.$m.'/'.$a;

                print '<tr>';
                print '<td>'.$movimiento.'</td>';
                print '<td>'.$actor_remite.'</td>';
                print '<td>'.$f_crea_nueva.'</td>';                
                print '<td>'.$actor_destino.'</td>';
                print '<td>'.$f_recepcion_nueva.'</td>';
                print '<td>'.$observaciones.'</td>'; 
                print '</tr>';                
            }
            print '</tbody>';            
            print '<table>';
        } else {
            print '</br>';
            print '<h5>No se encontro expediente</h5>';
            print '</br>';
        }    

    } else {
        print '</br>';
        print '<h5>No se encontro expediente</h5>';
        print '</br>';
    }
?>
<br>
<br>
<div class="text-center">
    <a href="index.php" class="btn btn-primary">Volver al Inicio</a>
</div>

</div>  <!-- container -->

<?php
}

include('php/foot.php');
include('script/script_index.php');
?>