<?php 
    session_start(); 
    define ('SITE_ROOT', realpath(dirname(__FILE__)));


include('expediente/head.php');

if (isset($_POST['enviar'])) {
   include('expediente/datos_post.php');
   include('expediente/persona.php');
   include('expediente/obtener_sector.php');
   include('expediente/obtener_actor.php');
   include('expediente/inserta_expediente.php');
   include('expediente/carga_archivos.php');
   include('expediente/mail_alumno.php');
   include('expediente/mail_secretario_academico.php');
   include('expediente/enviar_mail_secretario.php');
   include('expediente/enviar_mail_admisiones.php');
   
}//end if

include('expediente/mostrar_resumen.php');
?>
<br/>
<a href="index.php" class="btn btn-primary">Volver a Inicio </a>
</div><!-- container -->

<?php
    include('php/foot.php');    
    session_unset();
?>

<script type="text/javascript">
 toastr.options = {
            "tapToDismiss": false,
            "closeButton": false,
            "debug": false,
            "newestOnTop": false,
            "progressBar": false,
            "positionClass": "toast-bottom-right",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "30000",
            "hideDuration": "100000",
            "timeOut": "500000",
            "extendedTimeOut": "100000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
         }
toastr.info("<a href='consultar_expediente.php'> Consulte su expediente <b>  aqui</a> </b>")
</script>

<?php
function RemoveSpecialChar($str)
{
    $res =  preg_replace('([^A-Za-z0-9 ])', '', $str);
    return $res;
}

?>