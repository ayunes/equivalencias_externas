<?php
//--------------------------------------------------------------------------------------------------------------
//  FPDF
//---------------------------------------------------------------------------------------------------------------
require('vendor/fpdf/fpdf.php');
include('php/config.php');

//--- DATOS POST ------------------------------------------------------------------------------------------------
$id_sector          = $_GET['id_sector'];
$id_carrera         = $_GET['id_carrera'];
$modo_dest          = $_GET['modo_dest'];
$tipo_inst          = $_GET['tipo_inst'];
$inst_origen        = $_GET['inst_origen'];
$id_carrera_origen  = $_GET['id_carrera_origen'];
/* $plan_estudio       = urlencode($_GET['plan_estudio']); */
$plan_estudio       = $_GET['plan_estudio'];

//--- CURL MATERIAS -----------------------------------------------------------------------------------------------
$curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_URL => 'http://docker-server.ucasal.edu.ar:10010/v1/equivalencias-externas?institucion='.$inst_origen.'&tipo_institucion='.$tipo_inst.'&ext_carrera='.$id_carrera_origen .'&ext_plan='.$plan_estudio.'&sector='.$id_sector.'&carrera='.$id_carrera.'&modo='.$modo_dest,
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => '',
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 0,
  CURLOPT_FOLLOWLOCATION => true,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => 'GET',
));

$response = curl_exec($curl);

curl_close($curl);


$materias = json_decode($response, true);

//--- ARMAR PDF ----------------------------------------------------------------------------------------------------

class PDF extends FPDF
{
    
    function Header()
    {
        // Logo 
        $this->Image('media/banner_equivalencias.png',0,0,210); //10,8,33
        
        // Arial bold 15
        $this->SetFont('Arial','B',15);
        
        // Movernos a la derecha
        $this->Cell(80);
        
        // Título
        $this->Cell(30,37,'Listado de materias con equivalencias',0,0,'C');
        
        // Salto de línea
        $this->Ln(20);
    }

    // Pie de página
    function Footer()
    {
        // Posición: a 1,5 cm del final
        $this->SetY(-15);
        // Arial italic 8
        $this->SetFont('Arial','I',8);
        // Número de página
        $this->Cell(0,10,utf8_decode('Página ').$this->PageNo().'/{nb}',0,0,'C');
    }
}

// Creación del objeto de la clase heredada
$pdf = new PDF();
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Arial','B',12);

if ($materias != null) {
    $pdf->Cell(70,5,'',0,1);
    $pdf->Cell(80,5,'MATERIAS DE ORIGEN',0,0);
    $pdf->Cell(80,5,'MATERIAS DE UCASAL',0,0);
    $pdf->Cell(5,5,utf8_decode('CONDICIÓN'),0,1);
    $pdf->Cell(70,3,'',0,1);

    $pdf->SetFont('Arial','',8);
    
    $pdf->SetFillColor(224,235,255);    

    /* foreach ($materias['items'] as $k => $row) {
        $materia_origen  = substr(utf8_decode($row['materia_universidad_origen']), 0,45);
        $materia_destino = substr(utf8_decode($row['materia_ucasal']), 0,45);
        $codicion        = substr(utf8_decode(strtoupper($row['causa'])), 0,20);
        

        $pdf->Cell(80,5,$materia_origen,0,0);
        $pdf->Cell(80,5,$materia_destino,0,0);
        $pdf->Cell(5,5,$codicion,0,1);
    } */
    foreach ($materias as $row) {
        $materia_origen  = substr(utf8_decode($row['MATERIA_UNIVERSIDAD_ORIGEN']), 0,45);
        $materia_destino = substr(utf8_decode($row['MATERIA_UCASAL']), 0,45);
        $codicion        = substr(utf8_decode(strtoupper($row['CAUSA'])), 0,20);
        

        $pdf->Cell(80,5,$materia_origen,0,0);
        $pdf->Cell(80,5,$materia_destino,0,0);
        $pdf->Cell(5,5,$codicion,0,1);

    }
}

//--- Muestra PDF en Navegador ---------------------------------------------------
$pdf->Output();
?>