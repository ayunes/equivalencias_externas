<?php
session_start();

$id_sector          = $_SESSION['codigoSector'];
$id_carrera         = $_POST['id_carrera'];
$modo_dest          = $_POST['modo_dest'];
$tipo_inst          = $_POST['tipo_inst'];
$inst_origen        = $_POST['inst_origen'];
$id_carrera_origen  = $_POST['id_carrera_origen'];
$plan_estudio       = $_POST['plan_estudio'];

?>
<br>

<div class="contenedor_formulario">
    
    <div class="contenedor_titulo_formulario_red">
        <span class="titulo_formulario"> Completar los datos para continuar </span>              
    </div>

    <div class="form_admision">

        <div class="card-body">

            <div class="mb-3">
                <input type="text" class="form-control" id="nombre" name='nombre' value="" placeholder="Nombre" required=" required"> 
            </div>     
            
            <div class="mb-3">
                <input type="email" class="form-control" id="email" name='email' size="40" placeholder="Email" required>                
            </div>

            <div class="mb-3">
                <input type="text" class="form-control" id="n_celular" name='n_celular' placeholder="Nº teléfono(opcional)" >
            </div>

            <div class="mb-3">
                <button class="btn btn-danger" id="btn-numero3-info"  type="submit" name="enviar">CONSULTAR EQUIVALENCIAS</button>
            </div>

        </div><!-- card-body -->

    </div> <!-- form_selector --> 
    
</div><!-- contenedor formulario -->


<script>
    $(document).ready(function() {
        //$('html, body').animate( { scrollTop : 12000 }, 100 );
        $(location).attr('href','#solicitar_info'); 

        setTimeout(function() {
            $("#nombre").focus();
        }, 500);
      
    });
   
    $("#btn-numero3-info").on("click", function() {
        
        if ($("#nombre").val().length < 1 || $("#email").val().length < 1) {
            
            alert("Ingrese nombre y mail para continuar");
            
            setTimeout(function() {
                $("#nombre").focus();
            }, 500);

        } else {    

            $.ajax({
                type: "POST",
                url: "script/ajax_materias.php",
                data: {                        
                    "id_carrera": $('#lista1').val(),
                    "modo_dest": $('#lista2').val(),
                    "tipo_inst": $('#lista4').val(),
                    "inst_origen": $('#lista5').val(),
                    "id_carrera_origen": $('#lista6').val(),
                    "plan_estudio": $('#lista7').val(),
                    "nombre":$('#nombre').val(),
                    "email":$('#email').val(),
                    "n_celular":$('#n_celular').val()
                },
                success: function(r) {
                    $('#listar_materias').html(r);
                }
            });

        }//end if

    });
</script>