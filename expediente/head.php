<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title> Consultas de Equivalencias </title>
        
        <!-- Favicon-->
        <link rel="icon" type="image/x-icon" href="assets/favicon.ico" />
        
        <!-- Core theme CSS (includes Bootstrap)-->
        <link href="css/styles.css" rel="stylesheet" />     

        <script src="js/scripts.js"></script>
        
        <!-- TOAST -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.0.1/css/toastr.css" rel="stylesheet"/>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.0.1/js/toastr.js"></script>
        
        <!-- JQUERY -->
        <!--
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
        <script src="js/jquery3.js"></script>
        --> 
        
        <!-- Select2 Form -->        
        <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />        
        <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script> 
    </head>
    
    <body style="background-color:#f3f3f3;" class="hidden">
        
        <?php
           header ('Cache-Control: no-store, no-cache, must-revalidate, max-age = 0');
           include('php/nav.php');         
           include('php/config.php');
        ?>

    <div class="container">

    <br>
    <div class="col-auto text-center">
        <h3>Su trámite se realizó con éxito </h3>
    </div>
    <br>
    <ul>   
        <li>Se ha enviado un email a su casilla de correo con los datos ingresados.</li>
        <!-- <li>Puede consultar el seguimiento de su expediente desde el siguiente link -><a href="https://www-desa.ucasal.edu.ar/equivalencias_externas//consultar_expediente.php">consultar expediente</a>.</li> -->
    </ul>