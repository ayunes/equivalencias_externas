<?php
/*--------------------------------------------------------------------                 
        OBTENER MAIL DE SECRETARIO ACADEMICO
----------------------------------------------------------------------*/ 
    
switch ($codigo_sector) {
    case 2: // Esto se hardcodeo por que la dirección de email no esta vinculado al secretario académico
        $mail_sec = 'secretariaecademicaeconomia@ucasal.edu.ar';            
        break;
    
    case 172:  //si no encuentra actor envia mail a secretaria general
        $mail_sec = 'salonso@ucasal.edu.ar';
        break;

    case 752: //--- Si es por formulario de inscripción envia admisiones ------
        $mail_sec = 'apferrer@ucasal.edu.ar';        
        break;
    
    default:
        include('endpoints/obtener_mail_secretario_academico.php');      
        $mail_secretario = json_decode($response, true); 
        $mail_sec = 'ayunes@ucasal.edu.ar';
    
        foreach ($mail_secretario['items'] as $k => $row) {        
            $mail_sec = $row['email_institucional'];        
            break;
        }            
        break;
    
}//end switch


//print 'Mail Secretario Académico ---> '.$mail_sec .'</br>';



    /*-------------------------------------------------------------------
        Template Secretario Academico
    -------------------------------------------------------------------*/
    
    $nombre_usuario = $nombre.' '.$apellido;    

    $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_URL => 'http://template.ucasal.edu.ar/v1/equivalencias/facultad',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS =>'{
            "replaces":{
                "nombre_alumno":"'.$nombre_usuario.'",
                "n_docu":"'.$n_docu.'",
                "nombre_carrera":"'.$nombre_carrera.'",
                "nombre_modo":"'.$nombre_modo.'",
                "n_expediente":"'.$n_expediente.'",
                "anio":"'.$anio_expediente.'",
                "email":"'.$email_alumno.'",
                "observaciones":"'.$observaciones.'"                               
            }
        }',
        CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json'
    ),
    ));
    
    $template_sec_acad = curl_exec($curl);   
    
    curl_close($curl);
?>