<?php
/*
    ***************************************************************************************************
        INSERTAR SEG_EXPEDIENTE E INSERTO EXPEDIENTE SCH_SEGUIMIENTO.SEG_EXP_DETALLE y EXP_MOVIMS
    ***************************************************************************************************
        Debemos crear un autorCrea por ahora esta usando el 1 Alumno Equivalencia 
    ***************************************************************************************************
*/ 
    
/* Si observaciones esta en blanco guardo una - */
    if($observaciones == ''){
        $observaciones = '-';
    }

    //Oservaciones en seg_expedientes
    $observaciones = strval($observaciones);

    //Observaciones en seg_exp_detalle
    $obs_detalle = 'Carrera consulta:'.$nombre_carrera.' - Carrera Origen:'.$carrera_nombre_o;
   
    /* ORDS  Inserta los datos en SEG_EXPEDIENTE, SEG_EXP_MOVIMS y SEG_EXP_DETALLE */

    

    $curl = curl_init();   

    curl_setopt_array($curl, array(
        CURLOPT_URL => 'https://sistemas.ucasal.edu.ar/'.$srv.'/web/equivalencias-externas/createexpequiv/',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS =>'{
            "perIdRemite": "'.$id_persona.'",
            "obsExpediente": "'.$observaciones.'",
            "actorDestino": '.$codigo_actor.',
            "obsDetalle": "'.$obs_detalle.'"
        }',
            CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json'
            ),
    ));
    
    $response = curl_exec($curl);    
    
    curl_close($curl);
    
    $expediente = json_decode($response, true);

    $n_expediente = $expediente['idexp']; 
        
    $anio_expediente = date("Y");

    //print 'N° Expediente / Año ---> '.$n_expediente.' - '.$anio_expediente.'</br>';
?>