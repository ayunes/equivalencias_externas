<?php


//---------------------------------------------------------------------        
    /*  Busco Persona - ORDS - parametro: $n_docu */ 
    include('endpoints/bucar_persona.php');        
    $persona = json_decode($response, true);

    $c = 0;
    foreach ($persona['items'] as $k => $row) {
        $c = $c + 1;        
        $id_persona = $row['personaid'];        
    }    
    
    if ($c <= 0) {                           
        /********************************************
                Si no encuentra Persona JSON 
        **********************************************/
        include('endpoints/inserta_persona.php');     
        
        //--- Buscar Persona - ORDS --------------------
        include('endpoints/bucar_persona.php');  

        $persona = json_decode($response, true);
        
        $id_persona = 999;
        
        foreach ($persona['items'] as $k => $row) {        
           $id_persona = $row['personaid'];        
        }

        //--- Insertar Email - ORDS --------------------------------------------------------------        
        include('endpoints/inserta_mail.php');  
      
    } else {        
        //---  Si existe recuperamos ID de la persona ingresada  --------------------------
        
        //--- Buscar Persona - ORDS --------------------------------------------------------    
        include('endpoints/bucar_persona.php');  

        $persona = json_decode($response, true);        

        foreach ($persona['items'] as $k => $row) {            
            $id_persona = $row['personaid'];
        }
        
        //--- Recuperamos MAIL ---------------------------------------------------------------
        include('endpoints/recupera_mail.php');  
        $recuperaMmail = json_decode($response, true);        

        $mail_persona = "";
        foreach ($recuperaMmail as $row) { 
            $mail_persona = $row['mai_MAIL'];  
        }        
        
        //Si encuentra mail guardado
        if(isset($mail_persona)){            
            //Comparo el mail ingresado con el mail guardado en la BD.
            if (strcmp(strtolower($email_alumno), strtolower($mail_persona)) !== 0) {
                
                //Si no son iguales actualizo el mail de la bd por el ingresado                
                $curl = curl_init();

                curl_setopt_array($curl, array(
                    CURLOPT_URL => 'http://'.$html_link.'/equivalencias/update-mail',
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => '',
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 0,
                    CURLOPT_FOLLOWLOCATION => true,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => 'PUT',
                    CURLOPT_POSTFIELDS =>'{
                    "mailMail": "'.$email_alumno.'",
                    "per_id": '.$id_persona.'
                    }',
                    CURLOPT_HTTPHEADER => array(
                        'Content-Type: application/json'
                    ),
                ));

                $response = curl_exec($curl);

                curl_close($curl);
            }//end if
            
        } else {
            
            //---- Si no encuentra mail guardado - Insertar Email - ORDS ---------------------------------------
            include('endpoints/inserta_mail.php');  

        } //end if

    } //end if persona


    //print '<br/> persona ---> '.$id_persona.'</br>';
?>