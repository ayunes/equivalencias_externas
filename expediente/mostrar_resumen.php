<!-- <h5>Datos Personales</h5> -->
<table class="table table-strip"> 
    
    <tr>
        <th>Nº Expediente</th>
        <td><?=$n_expediente; ?></td>
    </tr>

    <tr>
        <th>Año</th>        
        <td><?=$anio_expediente; ?></td>
    </tr>
    
    <tr>
        <th>ID Personas </th>
        <td><?=$id_persona; ?></td>
    </tr>    
    
    <tr>
        <th>DNI: </th>
        <td><?=$n_docu; ?></td>
    </tr>

    <tr>
        <th>Nombre</th> 
        <td><?=$nombre; ?></td>        
    </tr>
    <tr>
        <th>Apellido</th>
        <td><?=$apellido; ?></td>
    </tr> 
    <tr>
        <th>Email</th> 
        <td><?=$email_alumno; ?></td>
    </tr>
    
    <tr>
        <th>Carrera</th>
        <td><?=$nombre_carrera; ?></td>        
    </tr>
    <tr>            
        <th>Modo</th>
        <td><?=$nombre_modo; ?></td>
    </tr>  
    

    <tr>
        <th>Observaciones SEG_EXP:</th>
        <td><?=$obs_detalle; ?></td>
    </tr>
   <!--
    <tr>
        <th>Codigo Actor</th>
        <td><?=$codigo_actor; ?></td>
    </tr>

    <tr>
        <th>Sector</th>
        <td><?=$codigo_sector; ?></td>        
    </tr>
    
    
    <tr>
        <th>Envia mail a: </th> 
        <td><?=$mail_sec; ?></td>
    </tr>
    -->
     
    <tr>
        <th>Detalle Carreras</th>
        <td><?= $obs_detalle; ?></td>
    </tr>
    
    <tr>
        <th>Observaciones</th>
        <td><?=$observaciones; ?></td>
    </tr>    

    <tr>
        <th>Plan de Estudio</th>
        <td><?=$data_plan; ?></td>
    </tr>

    <tr>
        <th>Certificado analitico</th>
        <td><?=$data_certificado; ?></td>
    </tr>

</table>