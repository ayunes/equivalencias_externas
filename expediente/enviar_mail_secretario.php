<?php
/*  Envio el email al Secretario Académico   */    

/****************************************************
    IMPORTANTE CAMBIAR MAIL AUXILIAR PARA PRODUCCION    
*****************************************************/

$mail_sec_acad = [
    "email"=> [$mail_sec],
    "subject"=>"Equivalencias Externas",
    "template" => $template_sec_acad,
    "replyTo"=>"no-reply@ucasal.edu.ar"    
];
    
$curl = curl_init();

curl_setopt_array($curl, array(
    CURLOPT_URL => 'http://tomeegen1.ucasal.edu.ar:8080/mailbackend/mail/enviar-mail',
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => '',
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 0,
    CURLOPT_FOLLOWLOCATION => true,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => 'POST',
    CURLOPT_POSTFIELDS => json_encode($mail_sec_acad),
    CURLOPT_HTTPHEADER => array(
        'Content-Type: application/json'
    ),
));

$response = curl_exec($curl);
curl_close($curl);

?>