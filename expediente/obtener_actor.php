<?php
/**************************************************************************************
    Si no encuentra actor en MEU va a secretaria general antes pasaba 0
    **************************************************************************************/
    $codigo_actor = 172; 

    /* Si el formulario es una inscripción envio a Admisiones  */
    if($tipo_form == 'inscripcion'){
        
        //--- Admisiones para Andrea F. ---------------------------
        $codigo_actor = 752;

    } else {

        //--- OBTENEMOS EL ACTOR (actorDestino) A PARTIR DEL SECTOR --------------------------                
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://sistemas.ucasal.edu.ar/'.$srv.'/web/equivalencias-externas/codigoactor/'.$codigo_sector.'',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        
        $c_sector = json_decode($response, true);

        foreach ($c_sector['items'] as $k => $row) {        
            $codigo_actor = $row['codigoactor'];                    
        }    
            
        /* Control si Facultad Económia es modo presencial o a distancia &&($codigo_carrera == 14)   */    
        if(($codigo_sector == 2)&&($nombre_modo=="PRESENCIAL")){
            $codigo_actor = 97;
        } elseif (($codigo_sector == 2)&&($nombre_modo !="PRESENCIAL")) {
            $codigo_actor = 62;
        }    

        

    }//end if control si formulario es incripcion

  //  print 'obtener actor ---> '.$codigo_actor.'</br>' ;
?>