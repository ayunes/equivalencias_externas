<?php
/*------------------------------------------------------------------------        
         INSERTAR ARCHIVO CERTIFICADO ANALITICO
    -------------------------------------------------------------------------*/
    $mes = date("m");
    $tam_max = 10485760;

    if  (
            ($_FILES['certificado']['name'] =='') ||
            ($_FILES['certificado']['type'] != 'application/pdf') ||
            ($_FILES['certificado']['size'] > $tam_max)
        )
    {
        
        $archivo_certificado = '-';    
        $tipo_certificado = '-';
        $tam_certificado = 0;
        $data_certificado = '-';      

    } else {

        $archivo_certificado = $_FILES['certificado']['name'];
        $tipo_certificado = $_FILES['certificado']['type'];
        $tam_certificado = $_FILES['certificado']['size'];
        $certificado_tmp = $_FILES['certificado']['tmp_name'];

        $archivo_certificado = $anio_expediente.$n_expediente . '_' . $archivo_certificado;
        $path = '/mnt/mesa_entrada/' . $anio_expediente . '/' . $mes.'/'; //http://wl12-desa.ucasal.edu.ar
       

        //Controlo si existe el directorio, para crearlo.
        $path_control = SITE_ROOT.'/mnt/mesa_entrada/' . $anio_expediente . '/' . $mes.'/';
        
        if (!file_exists($path_control)) {
            //print 'No Existe, creando....';
            mkdir($path_control, 0777, true);
        }    
        
        $path_certificado_archivo = SITE_ROOT.'/mnt/mesa_entrada/' . $anio_expediente . '/' . $mes. '/'.$archivo_certificado;                                      
        //print $path_certificado_archivo = '/mnt/mesa_entrada/' . $anio_expediente . '/' . $mes. '/'.$archivo_certificado;                                      
        
        /* Subir archivo al servidor */
        if (move_uploaded_file($certificado_tmp,$path_certificado_archivo)){
            $data_certificado = 'Certificado subido';

            /* ORDS */
            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => 'https://sistemas.ucasal.edu.ar/'.$srv.'/web/equivalencias-externas/createinsarchivo/',
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_POSTFIELDS =>'{
                    "codExpediente": ' . $n_expediente . ',
                    "docuNombre":"'. $archivo_certificado .'",
                    "docuTam":'.$tam_certificado .',
                    "docuPath":"'.$path.'",
                    "numeroMovim":1
                }',
                CURLOPT_HTTPHEADER => array(
                    'Content-Type: application/json'
                ),
            ));

            $response = curl_exec($curl);
            curl_close($curl);
            $data_certificado = 'Adjunto';

        } else {
            $data_certificado = 'Certificado no subido';
        }//end if
    }
          
    /*-------------------------------------------------------------------        
        SI EXISTE INSERTO ARCHIVO PLAN DE ESTUDIO
    ----------------------------------------------------------------------*/
     /* Plan de estudio */
     
     if (
            ($_FILES['plan']['name']=='') ||
            ($_FILES['plan']['type'] != 'application/pdf') ||
            ($_FILES['plan']['size'] > $tam_max)
        )
    {
        $archivo_plan = '-';
        $tipo_plan = '-';    
        $tam_plan = 0;        
        $data_plan = '-';
     } else {
        $archivo_plan = $_FILES['plan']['name'];
        $tipo_plan = $_FILES['plan']['type'];    
        $tam_plan = $_FILES['plan']['size'];
        $plan_tmp = $_FILES['plan']['tmp_name'];   

        $archivo_plan = $anio_expediente.$n_expediente . '_' . $archivo_plan;             
        
        $path = '/mnt/mesa_entrada/' . $anio_expediente . '/' . $mes.'/';       //. '/'.$archivo_plan http://wl12-desa.ucasal.edu.ar        
                
        $path_plan_archivo = SITE_ROOT.'/mnt/mesa_entrada/' . $anio_expediente . '/' . $mes. '/'.$archivo_plan;   
                        
        /* Subir Archivo al servidor */
        if (move_uploaded_file($plan_tmp,$path_plan_archivo)){
            $data_plan = 'Plan subido';
        
            /* ORDS - Registrar plan de estudio */
            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => 'https://sistemas.ucasal.edu.ar/'.$srv.'/web/equivalencias-externas/createinsarchivo/',
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_POSTFIELDS =>'{
                    "codExpediente": ' . $n_expediente . ',
                    "docuNombre": "' . $archivo_plan . '",
                    "docuTam":' . $tam_plan . ',
                    "docuPath":"' . $path . '",
                    "numeroMovim":1
                }',
                CURLOPT_HTTPHEADER => array(
                    'Content-Type: application/json'
                ),
            ));

            $response = curl_exec($curl);

            curl_close($curl);
            $data_plan = 'Adjunto';
        } else {
            $data_plan = 'Plan no subido';
        }
     }   

    //print 'Carga Archivos ---> ok </br>';
?>