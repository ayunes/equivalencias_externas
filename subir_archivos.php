<?php
    include('php/head.php');
    include('php/nav.php');
    define ('SITE_ROOT', realpath(dirname(__FILE__)));
?>

<div class="container">

<?php

if (isset($_POST['enviar'])) {
    $n_expediente = 11111; 
    $mes = 12;    
    $anio_expediente = date("Y");


    //Controlo si existe el directorio, para crearlo.
    $path_control = SITE_ROOT.'/mnt/mesa_entrada/' . $anio_expediente . '/' . $mes.'/';
        
    if (!file_exists($path_control)) {
        print 'No Existe, creando....';
        mkdir($path_control, 0777, true);
    }   else {
        print 'Directorio Existente';
    } 

   

    $archivo_certificado    = $_FILES['certificado']['name'];
    print '<br>';
    $tipo_certificado       = $_FILES['certificado']['type'];
    print '<br>';
    $tam_certificado        = $_FILES['certificado']['size'];
    print '<br>';
    $certificado_tmp        = $_FILES['certificado']['tmp_name'];
    print '<br>';

    $archivo_certificado = $anio_expediente.$n_expediente . '_' . $archivo_certificado;
    $path = '/mnt/mesa_entrada/' . $anio_expediente . '/' . $mes.'/';

       
    print $path_certificado_archivo = SITE_ROOT.'/mnt/mesa_entrada/' . $anio_expediente . '/' . $mes. '/'.$archivo_certificado;                                      
    print '<br>';
    
    /* Subir archivo al servidor */
    if (move_uploaded_file($certificado_tmp,$path_certificado_archivo)){
        print $data_certificado = 'Certificado subido';
    } else {
        print $data_certificado = 'Certificado no subido';
    }
    print '<br>';    
    print '------------------------------------------------------------------------------------';
    print '<br>';
    $archivo_plan = $_FILES['plan']['name'];
    print '<br>';
    $tipo_plan = $_FILES['plan']['type'];    
    print '<br>';
    $tam_plan = $_FILES['plan']['size'];
    print '<br>';
    $plan_tmp = $_FILES['plan']['tmp_name'];   
    print '<br>';
    $archivo_plan = $anio_expediente.$n_expediente . '_' . $archivo_plan;
    $path = '/mnt/mesa_entrada/' . $anio_expediente . '/' . $mes.'/';    
    print $path_plan_archivo = SITE_ROOT.'/mnt/mesa_entrada/' . $anio_expediente . '/' . $mes. '/'.$archivo_plan;   

    if (move_uploaded_file($plan_tmp,$path_plan_archivo)){
            print $data_plan = 'Plan subido';            
        } else {
            print $data_plan = 'Plan no subido';
        }
    

}//end if enviar
?>


    <form action='subir_archivos.php' method="POST" enctype="multipart/form-data">    
        <br/>
        <div class="mb-3">
            <div class='label-select'><b>Si posee el Certificado Analítico de su carrera adjuntelo aquí. </b> (solo archivos formato pdf, tamaño maximo: 10 mb) </div>
            <input class="form-control" type="file" id="formFile" name="certificado" accept="application/pdf">
        </div>

        <br />

        <div class="mb-3">
            <div class='label-select'><b>Si posee el Plan de Estudio de la Carrera adjuntelo aquí. </b> (solo archivos formato pdf, tamaño maximo: 10 mb) </div>
            <input class="form-control" type="file" id="formFile" name="plan" accept="application/pdf">
        </div>
        <br/>
        <input  type="submit" name="enviar" class="btn btn-primary"  />

    </form>


</div>

<?php
    include('php/foot.php');
    include('script/script_index.php');
?>
