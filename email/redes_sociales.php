<table class="module" role="module" data-type="social" align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;">
  <tbody>
    <tr>
      <td valign="top" style="padding:0px 0px 0px 0px;font-size:6px;line-height:10px;">
        <table align="center">
          <tbody>
            
            <tr>
              <td style="padding: 0px 5px;">
                <a role="social-icon-link"  href="https://www.facebook.com/Ucasal" target="_blank" alt="Facebook"
                  data-nolink="false"
                  title="Facebook "
                  style="-webkit-border-radius:17px;-moz-border-radius:17px;border-radius:17px;display:inline-block;background-color:#3B579D;">
                  <img role="social-icon" alt="Facebook" title="Facebook "
                    height="30"
                    width="30"
                    style="height: 30px, width: 30px"
                    src="https://marketing-image-production.s3.amazonaws.com/social/white/facebook.png" />
                </a>
              </td>

              <td style="padding: 0px 5px;">
                <a role="social-icon-link"  href="https://twitter.com/ucasaloficial" target="_blank" alt="Twitter"
                  data-nolink="false"
                  title="Twitter "
                  style="-webkit-border-radius:17px;-moz-border-radius:17px;border-radius:17px;display:inline-block;background-color:#7AC4F7;">
                  <img role="social-icon" alt="Twitter" title="Twitter "
                    height="30"
                    width="30"
                    style="height: 30px, width: 30px"
                    src="https://marketing-image-production.s3.amazonaws.com/social/white/twitter.png" />
                </a>
              </td>

              <td style="padding: 0px 5px;">
                <a role="social-icon-link"  href="https://www.instagram.com/ucasal" target="_blank" alt="Instagram"
                  data-nolink="false"
                  title="Instagram "
                  style="-webkit-border-radius:17px;-moz-border-radius:17px;border-radius:17px;display:inline-block;background-color:#7F4B30;">
                  <img role="social-icon" alt="Instagram" title="Instagram "
                    height="30"
                    width="30"
                    style="height: 30px, width: 30px"
                    src="https://marketing-image-production.s3.amazonaws.com/social/white/instagram.png" />
                </a>
              </td>

              <td style="padding: 0px 5px;">
                <a role="social-icon-link"  href="https://www.linkedin.com/school/universidad-cat-lica-de-salta/?viewAsMember=true" target="_blank" alt="LinkedIn"
                  data-nolink="false"
                  title="LinkedIn "
                  style="-webkit-border-radius:17px;-moz-border-radius:17px;border-radius:17px;display:inline-block;background-color:#0077B5;">
                  <img role="social-icon" alt="LinkedIn" title="LinkedIn "
                    height="30"
                    width="30"
                    style="height: 30px, width: 30px"
                    src="https://marketing-image-production.s3.amazonaws.com/social/white/linkedin.png" />
                </a>
              </td>
            </tr>
          </tbody>
        </table>
      </td>
    </tr>
  </tbody>
</table>