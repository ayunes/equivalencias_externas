<?php
include('php/head.php');
include('php/nav.php');
include('php/control_datos_ingreso.php');


$this_year = date("Y");
?>
<div class="container">
    <div class="col-auto text-center">
        <div class="card">
            <div class="card-header">
                <h4>CONSULTAR EXPEDIENTE</h4>
            </div>
            <div class="card-body">                    

                <form action="proc_consultar_expediente.php" method="POST">
                    
                    <div class="row">

                        <div class="col-md-5">
                            <div class='label-select'><b>DNI</b></div>
                            <input type="text" class="form-control" id="dniInput1" placeholder="Nº de DNI del solcitante" name='n_dni'  required autofocus>
                        </div>

                        <div class="col-md-5">
                            <div class='label-select'><b>Nº Expediente</b></div>
                            <input type="text" class="form-control" id="n_expInput2" placeholder="Nº de expediente" name='n_exp'  required>
                        </div>

                        <div class="col-md-2">
                            <div class='label-select'><b>Año</b></div>
                            <select name="anio" class="form-select" id="añoInput3" required>                                
                                <option value="<?=$this_year; ?>"><?=$this_year; ?></option>                                
                            </select>                            
                        </div>

                    </div><!-- row -->
                    <br>         
                    <div class="row">
                        <div class="col-md-12">
                            <!-- class="con_exp" -->
                            <button class="btn btn-primary" type="submit">CONSULTAR </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<?php
include('php/foot.php');
include('script/script_index.php');
?>